///////////////////////////////////////////////////////////////////////////////
//  IDrawPluginUpdater.h
//  Coder.Yudai Senoo :-)

#ifndef __IDRAWPLUGINPUDATER_H__
#define __IDRAWPLUGINPUDATER_H__

#include <objbase.h>

interface IDrawPluginUpdater {
	virtual void	Update(void) = 0;	// 画面の更新
};

#endif // __IDRAWPLUGINPUDATER_H__

