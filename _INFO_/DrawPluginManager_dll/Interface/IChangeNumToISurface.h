///////////////////////////////////////////////////////////////////////////////
//  IChangeNumToISurface.h
//  Coder.Yudai Senoo :-)

#ifndef __ICHANGENUMTOISURFACE_H__
#define __ICHANGENUMTOISURFACE_H__

#include <objbase.h>

interface ISurface;

interface IChangeNumToISurface {
	virtual ISurface*	Change(int nNum) = 0;	// 数値からISurface*への変換
};

#endif // __ICHANGENUMTOISURFACE_H__

