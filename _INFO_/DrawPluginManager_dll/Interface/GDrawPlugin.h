///////////////////////////////////////////////////////////////////////////////
//  GDrawPlugin.h
//  Coder.Yudai Senoo :-)

#ifndef __GDRAWPLUGIN_H__
#define __GDRAWPLUGIN_H__

#include <objbase.h>

// {21D5AB5C-B8BB-4cb3-A238-53BE3EC7684D}
static const GUID IID_IDrawPlugin = 
{ 0x21d5ab5c, 0xb8bb, 0x4cb3, { 0xa2, 0x38, 0x53, 0xbe, 0x3e, 0xc7, 0x68, 0x4d } };

#endif // __GDRAWPLUGIN_H__

