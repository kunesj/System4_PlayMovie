///////////////////////////////////////////////////////////////////////////////
//  GDrawPluginManager.h
//  Coder.Yudai Senoo :-)

#ifndef __GDRAWPLUGINMANAGER_H__
#define __GDRAWPLUGINMANAGER_H__

#include <objbase.h>

// {AA3ACE45-1AE3-4ed6-A50C-458353403842}
static const GUID IID_IDrawPluginManager = 
{ 0xaa3ace45, 0x1ae3, 0x4ed6, { 0xa5, 0xc, 0x45, 0x83, 0x53, 0x40, 0x38, 0x42 } };

// {7EE68A63-F695-4e2c-A82F-556486DCBBDD}
static const GUID IID_IDrawPluginUpdater = 
{ 0x7ee68a63, 0xf695, 0x4e2c, { 0xa8, 0x2f, 0x55, 0x64, 0x86, 0xdc, 0xbb, 0xdd } };

// {2EB2287F-9625-48dd-A1A6-3BD3EDB2F082}
static const GUID IID_IDrawPluginDrawer = 
{ 0x2eb2287f, 0x9625, 0x48dd, { 0xa1, 0xa6, 0x3b, 0xd3, 0xed, 0xb2, 0xf0, 0x82 } };

#endif // __GDRAWPLUGINMANAGER_H__

