///////////////////////////////////////////////////////////////////////////////
//  GDrawPluginUpdater.h
//  Coder.Yudai Senoo :-)

#ifndef __GDRAWPLUGINUPDATER_H__
#define __GDRAWPLUGINUPDATER_H__

#include <objbase.h>

// {7EE68A63-F695-4e2c-A82F-556486DCBBDD}
static const GUID IID_IDrawPluginUpdater = 
{ 0x7ee68a63, 0xf695, 0x4e2c, { 0xa8, 0x2f, 0x55, 0x64, 0x86, 0xdc, 0xbb, 0xdd } };

#endif // __GDRAWPLUGINUPDATER_H__

