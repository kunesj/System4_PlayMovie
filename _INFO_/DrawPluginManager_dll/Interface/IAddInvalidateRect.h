///////////////////////////////////////////////////////////////////////////////
//  IAddInvalidateRect.h
//  Coder.Yudai Senoo :-)

#ifndef __IADDINVALIDATERECT_H__
#define __IADDINVALIDATERECT_H__

#include <objbase.h>

interface IAddInvalidateRect {
	virtual void	Add(int nNum, int nX, int nY, int nWidth, int nHeight) = 0;	// �����̈�̒ǉ�
};

#endif // __IADDINVALIDATERECT_H__

