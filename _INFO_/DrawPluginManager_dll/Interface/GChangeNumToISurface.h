///////////////////////////////////////////////////////////////////////////////
//  GChangeNumToISurface.h
//  Coder.Yudai Senoo :-)

#ifndef __GCHANGENUMTOISURFACE_H__
#define __GCHANGENUMTOISURFACE_H__

#include <objbase.h>

// {631954D8-253C-4cd6-88B2-3AAB75923B64}
static const GUID IID_IChangeNumToISurface = 
{ 0x631954d8, 0x253c, 0x4cd6, { 0x88, 0xb2, 0x3a, 0xab, 0x75, 0x92, 0x3b, 0x64 } };

#endif // __GCHANGENUMTOISURFACE_H__

