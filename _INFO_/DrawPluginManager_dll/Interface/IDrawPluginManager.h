///////////////////////////////////////////////////////////////////////////////
//  IDrawPluginManager.h
//  Coder.Yudai Senoo :-)

#ifndef __IDRAWPLUGINMANAGER_H__
#define __IDRAWPLUGINMANAGER_H__

#include "IInterface.h"

interface IDrawPluginManager : public IInterface {
	virtual void	SetInterface(const GUID* pGuid, void* pvInterface) = 0;		// インターフェイスの設定
	virtual void*	GetInterface(const GUID* pGuid) = 0;						// インターフェイスの取得
};

#endif // __IDRAWPLUGINMANAGER_H__

