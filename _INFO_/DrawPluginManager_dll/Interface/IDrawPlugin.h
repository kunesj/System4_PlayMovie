///////////////////////////////////////////////////////////////////////////////
//  IDrawPlugin.h
//  Coder.Yudai Senoo :-)

#ifndef __IDRAWPLUGIN_H__
#define __IDRAWPLUGIN_H__

#include "IInterface.h"

interface ISurface;

interface IDrawPlugin : public IInterface {
	virtual void	SetInterface(const GUID* pGuid, void* pvInterface) = 0;		// インターフェイスの設定

	virtual void	Update(void) = 0;	// 画面の更新
	virtual void	Draw(int nSprite, ISurface* pISurface, int nDx, int nDy, int nSx, int nSy, int nWidth, int nHeight, int nAlpha) = 0;	// 描画

};

#endif // __IDRAWPLUGIN_H__

