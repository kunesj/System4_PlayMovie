///////////////////////////////////////////////////////////////////////////////
//  IDrawPluginDrawer.h
//  Coder.Yudai Senoo :-)

#ifndef __IDRAWPLUGINDRAWER_H__
#define __IDRAWPLUGINDRAWER_H__

#include <objbase.h>

interface ISurface;

interface IDrawPluginDrawer {
	virtual void	Draw(int nSprite, ISurface* pISurface, int nDx, int nDy, int nSx, int nSy, int nWidth, int nHeight, int nAlpha) = 0;
};

#endif // __IDRAWPLUGINDRAWER_H__

