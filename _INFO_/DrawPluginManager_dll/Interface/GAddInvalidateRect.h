///////////////////////////////////////////////////////////////////////////////
//  GAddInvalidateRect.h
//  Coder.Yudai Senoo :-)

#ifndef __GADDINVALIDATERECT_H__
#define __GADDINVALIDATERECT_H__

#include <objbase.h>

// {1C225D59-DA0D-438b-8481-0DA06A4D89E6}
static const GUID IID_IAddInvalidateRect = 
{ 0x1c225d59, 0xda0d, 0x438b, { 0x84, 0x81, 0xd, 0xa0, 0x6a, 0x4d, 0x89, 0xe6 } };

#endif // __GADDINVALIDATERECT_H__

