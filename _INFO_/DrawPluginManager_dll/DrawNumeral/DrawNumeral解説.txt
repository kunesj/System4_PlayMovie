.DrawNumeral

..DrawNumeralの用途
DrawNumeralはSACT上で数字をＣＧで表示したい場合に使用します。
従来のスプライトに数字ＣＧをコピーする方法と比較して、ほとん
どの場合メモリ消費量、描画速度ともにDrawNumeralを使用したほう
が有利になると思われます。

..クラス作成例
このフォルダ内のCDrawNumeral.jafにDrawNumeralを使用した
クラスの作成例があるので参考にしてください。

..HLL関数

...LoadCG
bool LoadCG(int nCG);
数字ＣＧをロードします。
nCG+0,nCG+1,...,nCG+9の１０枚のＣＧが使用されるので、
nCG+0が"0",...,nCG+9が"9"となるように連番で１０枚
用意してください。
LoadCGは重複して呼び出しても構いません。既にロードされている
ＣＧを再ロードしようとした場合、何もせずに戻ります。

...UnloadCG
bool UnloadCG(int nCG);
LoadCGでロードしたＣＧを削除します。
どうしてもメモリが不足する場合以外は通常は呼び出さないでください。

...Register
bool Register(int nSp, int nCG, int nFigure, bool bZeroPadding);
DrawNumeralによって描画されるスプライトを登録します。
スプライトはSACTのSP_CREATE_CUSTOM()によって作成されたモノである
必要があります。
nCGへは、LoadCG()でロードしたＣＧを指定してください。
nFigureは桁数を指定します。0は指定できません。
bZeroPaddingは先頭を0で埋める場合はtrueを、空白で生める場合は
falseを指定してください。

...Unregister
bool Unregister(int nSp);
Registerで登録したスプライトの登録を削除します。
スプライト自体は削除されません。
SACTのSP_DEL()と合わせて呼び出してください。

...SetValue
bool SetValue(int nSp, int nValue);
表示する値を設定します。
nSpはRegisterで登録したスプライトを指定してください。

...GetValue
int GetValue(int nSp);
SetValue()で設定した値を取得します。
nSpはRegisterで登録したスプライトを指定してください。

...SetBrightness
bool SetBrightness(int nSp, int nBrightness);
表示する数字の明るさを指定します。
nSpはRegisterで登録したスプライトを指定してください。
nBrightness=0のとき真っ黒に、nBrightness=255のとき元のＣＧの
明るさで表示されます。
【補足】
ブレンド率を指定する場合は、従来どおりSACTのSP_SET_BLENDRATE()
を使用してください。
