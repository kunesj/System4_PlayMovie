【 DrawRipple解説書 】

★サンプルコード

DrawPluginManager.Load("DrawRipple");

DrawRipple.Init(nSp);
DrawRipple.Start(nSp);
DrawRipple.SetClipAMap(nSp, nAlphaSp);	// クリッピングスプライト登録

...

DrawRipple.Release(nSp);


★初期値

Numof = 384
SizeWidth = 6
SizeHeight = 3
TotalTime = 100


