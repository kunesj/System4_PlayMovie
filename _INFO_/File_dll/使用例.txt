File.dll 使用例

File.dllは構造体のセーブ・ロードを行います。
構造体 Data_t を使った例です。

セーブ例

	Data_t tData;
	
	szName = system.GetSaveFolderName() + "\\記録ファイル.asd";
	if (File.Open(szName, 2))
	{
		File.Write(tData);
		File.Close();
	}
	else
	{
		system.MsgBox("セーブエラー");
	}

ロード例

	Data_t tData;
	
	szName = system.GetSaveFolderName() + "\\記録ファイル.asd";
	if (File.Open(szName, 1))
	{
		File.Read(tData);
		File.Close();
	}
	else
	{
		system.MsgBox("ロードエラー");
	}
