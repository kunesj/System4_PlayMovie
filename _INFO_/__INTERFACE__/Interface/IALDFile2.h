///////////////////////////////////////////////////////////////////////////////
//  IALDFile2.h
//  Coder.Yudai Senoo :-)

#ifndef __IALDFILE2_H__
#define __IALDFILE2_H__

#include "IALDFile.h"

interface IString;
interface IALDFile2 : public IALDFile {
	// データ位置のファイル名・オフセット位置・サイズを取得する
	virtual bool	GetDataPos(int nNum, IString* pIFileName, int* pnOffset, int* pnSize) = 0;
	
};

#endif // __IALDFILE2_H__

