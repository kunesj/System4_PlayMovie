///////////////////////////////////////////////////////////////////////////////
//  IJoypad.h
//  Coder.Yudai Senoo :-)

#ifndef __IJOYPAD_H__
#define __IJOYPAD_H__

#include <objbase.h>

interface IJoypad {
	virtual void	ClearKeyDownFlag(int nNum) = 0;
	
	virtual bool	IsKeyDown(int nNum, int nKey) = 0;

	virtual int		GetNumof(void) = 0;
};

/*
const int
	JK_LEFT			= 1,
	JK_RIGHT		= 2,
	JK_UP			= 3,
	JK_DOWN			= 4,
	JK_BUTTON1		= 5,
	JK_BUTTON2		= 6,
	JK_BUTTON3		= 7,
	JK_BUTTON4		= 8,
	JK_BUTTON5		= 9,
	JK_BUTTON6		= 10,
	JK_BUTTON7		= 11,
	JK_BUTTON8		= 12;
};
*/

#endif // __IJOYPAD_H__

