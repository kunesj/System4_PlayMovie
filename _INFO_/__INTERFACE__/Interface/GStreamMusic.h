///////////////////////////////////////////////////////////////////////////////
//  GSTREAMMusic.h
//  Coder.TYA-PA- :-)

#ifndef __GSTREAMMUSIC_H__
#define __GSTREAMMUSIC_H__

#include <objbase.h>

// {E733AE95-AC25-4e14-A053-893D0BA85A4B}
static const GUID IID_IMusicChannelGet = 
{ 0xe733ae95, 0xac25, 0x4e14, { 0xa0, 0x53, 0x89, 0x3d, 0xb, 0xa8, 0x5a, 0x4b } };

// {2C46570E-4695-40bb-9F89-D89048AF7027}
static const GUID IID_IStreamMusic = 
{ 0x2c46570e, 0x4695, 0x40bb, { 0x9f, 0x89, 0xd8, 0x90, 0x48, 0xaf, 0x70, 0x27 } };


// {68BD8586-588D-45eb-8625-2BFA048A51E2}
static const GUID IID_IMusicSamplePos = 
{ 0x68bd8586, 0x588d, 0x45eb, { 0x86, 0x25, 0x2b, 0xfa, 0x4, 0x8a, 0x51, 0xe2 } };

// {461E69D5-70D4-48fc-9844-20CFBB01144A}
static const GUID IID_IMusicLoopPos = 
{ 0x461e69d5, 0x70d4, 0x48fc, { 0x98, 0x44, 0x20, 0xcf, 0xbb, 0x1, 0x14, 0x4a } };

// {3D28BC1D-9D1A-4690-9FC1-8D169B5822C4}
static const GUID IID_IMusicSeek = 
{ 0x3d28bc1d, 0x9d1a, 0x4690, { 0x9f, 0xc1, 0x8d, 0x16, 0x9b, 0x58, 0x22, 0xc4 } };

// {977AE78E-AEDA-472e-8DE8-9BEB3DA696DC}
static const GUID IID_IStreamMusicMasterVolume = 
{ 0x977ae78e, 0xaeda, 0x472e, { 0x8d, 0xe8, 0x9b, 0xeb, 0x3d, 0xa6, 0x96, 0xdc } };

#endif // __GSTREAMMUSIC_H__
