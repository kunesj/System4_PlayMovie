///////////////////////////////////////////////////////////////////////////////
//  ITimer.h
//  Coder.Yudai Senoo :-)

#ifndef __ITIMER_H__
#define __ITIMER_H__

#include <objbase.h>

interface ITimer {
	virtual void			Init(void) = 0;		// タイマー初期化
	virtual unsigned int	Get(void) = 0;		// 時間取得
};

#endif // __ITIMER_H__

