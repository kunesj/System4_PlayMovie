///////////////////////////////////////////////////////////////////////////////
//  ISys4xPath.h
//  Coder.Yudai Senoo :-)

#ifndef __ISYS4XPATH_H__
#define __ISYS4XPATH_H__

#include <objbase.h>

interface ISys4xPath {
	virtual char*	GetCurrentPath(void) = 0;		// カレントパスを取得
	virtual char*	GetExeFilePath(void) = 0;		// 実行ファイルのあるパスを取得
	virtual char*	GetSaveFilePath(void) = 0;		// セーブファイルを保存するパスを取得
/*
	パス文字列の最後には必ず'\'が付きます
*/
};

#endif // __ISYS4XPATH_H__

