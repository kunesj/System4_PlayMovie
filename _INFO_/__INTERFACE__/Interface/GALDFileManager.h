///////////////////////////////////////////////////////////////////////////////
//  GALDFileManager.h
//  Coder.Yudai Senoo :-)

#ifndef __GALDFILEMANAGER_H__
#define __GALDFILEMANAGER_H__

#include <objbase.h>

// {FEFB86C3-EBEC-4c55-BC61-5223CB508664}
static const GUID IID_IALDFileManager = 
{ 0xfefb86c3, 0xebec, 0x4c55, { 0xbc, 0x61, 0x52, 0x23, 0xcb, 0x50, 0x86, 0x64 } };

// {4B7121CC-A170-4895-8528-3B0A0E888498}
static const GUID IID_IALDFileManager2 = 
{ 0x4b7121cc, 0xa170, 0x4895, { 0x85, 0x28, 0x3b, 0xa, 0xe, 0x88, 0x84, 0x98 } };

// {18AF0AAD-ED43-49fe-A17A-6575A1D82EBD}
static const GUID IID_IALDFileManager3 = 
{ 0x18af0aad, 0xed43, 0x49fe, { 0xa1, 0x7a, 0x65, 0x75, 0xa1, 0xd8, 0x2e, 0xbd } };

#endif // __GALDFILEMANAGER_H__

