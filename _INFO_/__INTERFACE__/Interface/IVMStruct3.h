///////////////////////////////////////////////////////////////////////////////
//  IVMStruct3.h
//  Coder.Yudai Senoo :-)

#ifndef __IVMSTRUCT3_H__
#define __IVMSTRUCT3_H__

#include "IVMStruct2.h"

interface IMemory;
interface IVMStruct3 : public IVMStruct2 {
	// メンバ名からインデックスの取得
	// エラーの場合は(-1)が返ります
	virtual int		GetIndexByName(char* pszMember) = 0;
	
	// 構造体同士のコピー
	// 現バージョンではメンバがint,float,string,struct,bool,functype
	// のみで構成される構造体以外はコピーできません
	virtual bool	Copy(IVMStruct3* pISrcStruct) = 0;

	// セーブ
	virtual IMemory*	Save(void) = 0;

	// ロード
	virtual bool		Load(void* pvData, int nSize) = 0;

};

#endif // __IVMSTRUCT3_H__

