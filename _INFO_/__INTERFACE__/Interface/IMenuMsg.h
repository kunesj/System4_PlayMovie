///////////////////////////////////////////////////////////////////////////////
//  IMenuMsg.h
//  Coder.Yudai Senoo :-)

#ifndef __IMENUMSG_H__
#define __IMENUMSG_H__

#include <objbase.h>

interface IMenuMsg {
	virtual void	SetState(bool bFlag) = 0;		// on/offの設定
	virtual void	SetEnable(bool bEnable) = 0;	// 有効/無効化の設定
	
	virtual bool	GetState(void) = 0;				// on/off状態の取得
	virtual bool	GetEnable(void) = 0;			// 有効/無効化状態の取得
};

#endif // __IMENUMSG_H__

