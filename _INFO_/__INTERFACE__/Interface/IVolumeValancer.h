///////////////////////////////////////////////////////////////////////////////
//  IVolumeValancer.h
//  Coder.Yudai Senoo :-)

#ifndef __IVOLUMEVALANCER_H__
#define __IVOLUMEVALANCER_H__

#include <objbase.h>

interface IChangeVolumeCallback;

interface IVolumeValancer {
	virtual void	SetNumof(int nNumof) = 0;				// バランサーの個数
	virtual void	SetName(int nNum, char* pszName) = 0;
	virtual void	SetVolume(int nNum, int nVolume) = 0;	// nVolume(0〜100)
	virtual void	SetMute(int nNum, bool bFlag) = 0;
	virtual void	SetDisable(int nNum, bool bFlag) = 0;

	virtual int		GetNumof(void) = 0;
	virtual char*	GetName(int nNum) = 0;
	virtual int		GetVolume(int nNum) = 0;
	virtual bool	GetMute(int nNum) = 0;
	virtual bool	GetDisable(int nNum) = 0;

	virtual void	AddCallbackInterface(IChangeVolumeCallback* pIChangeVolumeCallback) = 0;
};

#endif // __IVOLUMEVALANCER_H__

