///////////////////////////////////////////////////////////////////////////////
//  IMusicChannelGet.h
//  Coder. TYA-PA-

//チャンネル指定でIMusicを取得するインターフェイス


#ifndef __IMUSICCHANNELGET_H__
#define __IMUSICCHANNELGET_H__

#include "IInterface.h"

interface IMusic;

interface IMusicChannelGet : public IInterface {
	virtual IMusic*		Get(int nCh) = 0;

};

#endif // __IMUSICCHANNELGET_H__

