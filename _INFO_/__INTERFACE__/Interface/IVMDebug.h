///////////////////////////////////////////////////////////////////////////////
//  IVMDebug.h
//  Coder.Yudai Senoo :-)

#ifndef __IVMDEBUG_H__
#define __IVMDEBUG_H__

#include <objbase.h>

typedef unsigned long DWORD;

interface IVMDebug {
	virtual DWORD	GetIP(void) = 0;		// ���݂̂h�o���擾
	
	// �u�l����
	virtual void	Pause(void) = 0;		// �u�l�̈ꎞ��~
	virtual void	Restart(void) = 0;		// �ꎞ��~����
	
	virtual void	Step(void) = 0;			// �P���߂̂ݎ��s
};

#endif // __IVMDEBUG_H__

