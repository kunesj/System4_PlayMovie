///////////////////////////////////////////////////////////////////////////////
//  IVMGlobal.h
//  Coder.Yudai Senoo :-)

#ifndef __IVMGLOBAL_H__
#define __IVMGLOBAL_H__

#include <objbase.h>

interface IVMGlobal {
	// 取得
	virtual char*	GetName(int nIndex) = 0;	// 変数の名前を取る
	virtual int		GetNumof(void) = 0;		// 変数の個数

	// メンバオブジェクト取得
	virtual int		GetType(int nIndex) = 0;
	virtual void*	GetData(int nMember) = 0;

	//GetTypeで返ってくる値と、GetDataで取得できるポインタ種の対応表
	//	OBJTYPE_INT		int*
	//	OBJTYPE_FLOAT	float*
	//	OBJTYPE_STRING	IVMString
	//	OBJTYPE_STRUCT	IVMStruct
	//	OBJTYPE_A_****	IVMArray

	// メンバオブジェクト設定1
	virtual bool	SetData(int nMember, void* pvData) = 0;
	//GetTypeで返ってくる値と、SetDataで設定する内容の対応表
	//	OBJTYPE_INT		int*
	//	OBJTYPE_FLOAT	float*
	//	OBJTYPE_STRING	char*
	//	OBJTYPE_STRUCT	不可
	//	OBJTYPE_A_****	不可
};

#endif // __IVMSTRUCT_H__

