///////////////////////////////////////////////////////////////////////////////
//  IMusicPause.h
//  Coder.Yudai Senoo :-)

#ifndef __IMUSICPAUSE_H__
#define __IMUSICPAUSE_H__

#include <objbase.h>

interface IMusicPause {
	virtual bool	Pause(void) = 0;		// 一時停止
	virtual bool	Restart(void) = 0;		// 再開
	virtual bool	IsPause(void) = 0;		// 一時停止中かどうか
};

#endif // __IMUSICPAUSE_H__

