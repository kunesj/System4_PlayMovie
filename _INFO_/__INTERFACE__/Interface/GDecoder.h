///////////////////////////////////////////////////////////////////////////////
//  GDecoder.h
//  Coder.Yudai Senoo :-)

#ifndef __GDECODER_H__
#define __GDECODER_H__

#include <objbase.h>

// {BEEEA357-F270-49ec-BEC9-2D74DC0FEC56}
static const GUID IID_IDecoder = 
{ 0xbeeea357, 0xf270, 0x49ec, { 0xbe, 0xc9, 0x2d, 0x74, 0xdc, 0xf, 0xec, 0x56 } };

#endif // __GDECODER_H__

