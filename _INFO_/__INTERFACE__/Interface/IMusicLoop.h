///////////////////////////////////////////////////////////////////////////////
//  IMusicLoop.h
//  Coder.Yudai Senoo :-)

#ifndef __IMUSICLOOP_H__
#define __IMUSICLOOP_H__

#include <objbase.h>

interface IMusicLoop {
	virtual bool	SetCount(int nCount) = 0;		// ループ回数の設定
	virtual int		GetCount(void) = 0;				// ループ回数の取得
};

#endif // __IMUSICLOOP_H__

