///////////////////////////////////////////////////////////////////////////////
//  IGrEffect.h
//  Coder.Yudai Senoo :-)

#ifndef __IGREFFECT_H__
#define __IGREFFECT_H__

#include "IInterface.h"

interface ISurface;

interface IGrEffect : public IInterface {
	virtual void	Blend(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nAlpha) = 0;
	virtual void	BlendScreen(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;

	virtual void	InitBlendUseAMap(void) = 0;		// BlendUseAMapの初期化
	virtual void	BlendUseAMap(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, ISurface* pAlphaSurface, int nAx, int nAy, int nAlpha) = 0;		// αマップ参照ブレンド

	virtual void	SaturWhiteAMap(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, ISurface* pAlphaSurface, int nAx, int nAy, int nAlpha) = 0;		// Write = Satur(Src + AMap * Alpha)
	virtual void	SaturColor(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nR, int nG, int nB) = 0;		// Write = Satur(Src + Color)

};

#endif // __IGREFFECT_H__

