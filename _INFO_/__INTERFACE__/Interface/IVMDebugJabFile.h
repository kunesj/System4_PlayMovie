///////////////////////////////////////////////////////////////////////////////
//  IVMDebugJabFile.h
//  Coder.TYA-PA-

#ifndef __IVMDEBUGJABFILE_H__
#define __IVMDEBUGJABFILE_H__

#include <objbase.h>

interface IVMDebugJabFile {
	virtual char*	GetFuncName(int nNum) = 0;						// 関数の名前を取得
	virtual char*	GetStructName(int nNum) = 0;					// 構造体の名前を取得

	virtual int		GetFuncVarNumof(int nNum) = 0;					// 関数内にある変数の数を取得
	virtual int		GetFuncVarObjType(int nNum, int nVar) = 0;		// 関数内にある変数の型を取得
	virtual char*	GetFucVarName(int nNum, int nVar) = 0;			// 関数内にある変数の名前取得

	virtual int		GetGlobalVarNumof(void) = 0;					// グローバルオブジェクトにある変数の数を取得
	virtual int		GetGlobalVarObjType(int nVar) = 0;				// グローバルオブジェクトにある変数の型を取得
	virtual char*	GetGlobalVarName(int nVar) = 0;					// グローバルオブジェクトにある変数の名前を取得

	virtual int		GetStructVarNumof(int nNum) = 0;				// 構造体にある変数の数を取得
	virtual int		GetStructVarObjType(int nNum, int nVar) = 0;	// 構造体にある変数の型を取得
	virtual char*	GetStructVarName(int nNum, int nVar) = 0;		// 構造体にある変数の名前を取得
};

#endif //__IVMDEBUGJABFILE_H__

