///////////////////////////////////////////////////////////////////////////////
//  ISurfaceFactory.h
//  Coder.Yudai Senoo :-)

#ifndef __ISURFACEFACTORY_H__
#define __ISURFACEFACTORY_H__

#include "IInterface.h"

interface ISurface;

interface ISurfaceFactory : public IInterface {
	virtual ISurface*	Create(int nWidth, int nHeight, int nBpp) = 0;
	virtual ISurface*	CreatePixelOnly(int nWidth, int nHeight, int nBpp) = 0;
	virtual ISurface*	CreateAlphaOnly(int nWidth, int nHeight) = 0;
};

#endif // __ISURFACEFACTORY_H__

