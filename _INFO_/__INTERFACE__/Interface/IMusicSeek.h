///////////////////////////////////////////////////////////////////////////////
//  IMusicSeek.h
//  Coder.TYA-PA-

//再生位置を指定する、シーク用途

#ifndef __IMUSICSEEK_H__
#define __IMUSICSEEK_H__

#include <objbase.h>

typedef unsigned long DWORD;

interface IMusicSeek {
	virtual bool	Seek(DWORD dwPos) = 0;		// 再生位置の変更
	virtual DWORD	GetLength(void) = 0;	// 曲全体のサンプル数を取得
};

#endif // __IMUSICSEEK_H__

