///////////////////////////////////////////////////////////////////////////////
//  IMusicPos.h
//  Coder.Yudai Senoo :-)

#ifndef __IMUSICPOS_H__
#define __IMUSICPOS_H__

#include <objbase.h>

interface IMusicPos {
	virtual unsigned int	GetPos(void) = 0;		// 再生位置（ミリセカンド）の取得
	virtual unsigned int	GetLength(void) = 0;	// 再生時間の長さを取得
};

#endif // __IMUSICPOS_H__

