///////////////////////////////////////////////////////////////////////////////
//  IInputDevice.h
//  Coder.Yudai Senoo :-)

#ifndef __IINPUTDEVICE_H__
#define __IINPUTDEVICE_H__

#include <objbase.h>

interface IInputDevice {
	virtual void	ClearKeyDownFlag(void) = 0;

	virtual bool	IsKeyDown(int nKey) = 0;

	virtual bool	GetCursorPos(int* pnX, int* pnY) = 0;
	virtual bool	SetCursorPos(int nX, int nY) = 0;
};

#endif // __IINPUTDEVICE_H__

