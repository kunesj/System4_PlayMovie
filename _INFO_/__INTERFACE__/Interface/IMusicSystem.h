#ifndef __IMUSICSYSTEM_H__
#define __IMUSICSYSTEM_H__
//IMusicSystem.h
//kazushi

#include "IInterface.h"
#include <objbase.h>

interface IMusic;
interface IDSPointer;
interface ITimer;

interface IMusicSystem : public IInterface
{
	virtual void	Init(IDSPointer* pIDSPointer, ITimer* pITimer) = 0;
	virtual IMusic	*GetIMusic(int nCh) = 0;//チャンネル指定でIMusicを取得する
	virtual bool	ExistData(int nIndex) = 0;//指定したリンク番号のデーターが存在するか？

	virtual bool	SetMasterVolume(float fPercent)=0;		//基本音量設定 0〜100(%)
	virtual bool	GetMasterVolume(float* pfPercent)=0;	//基本音量取得 0〜100(%)
	virtual bool	SetMuteStatus(bool bMute)=0;			//ミュート設定
	virtual bool	GetMuteStatus(bool* pbMute)=0;			//ミュート状態取得
};

#endif // __IMUSICSYSTEM_H__
