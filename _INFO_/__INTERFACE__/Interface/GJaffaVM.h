///////////////////////////////////////////////////////////////////////////////
//  GJaffaVM.h
//  Coder.Tya-Pa-

#ifndef __GJAFFAVM_H__
#define __GJAFFAVM_H__

#include <objbase.h>

// {4CD9EBA2-DE24-462c-AA48-8D73EA5E55ED}
static const GUID IID_IVMDebugPage = 
{ 0x4cd9eba2, 0xde24, 0x462c, { 0xaa, 0x48, 0x8d, 0x73, 0xea, 0x5e, 0x55, 0xed } };

// {C005D7FC-F815-4d2b-9B6A-575EFABE1C9F}
static const GUID IID_IVMDebug = 
{ 0xc005d7fc, 0xf815, 0x4d2b, { 0x9b, 0x6a, 0x57, 0x5e, 0xfa, 0xbe, 0x1c, 0x9f } };

// {C9EB442C-0BBD-4431-BCAE-9C5423A66D61}
static const GUID IID_IVMSetTraceCallback = 
{ 0xc9eb442c, 0xbbd, 0x4431, { 0xbc, 0xae, 0x9c, 0x54, 0x23, 0xa6, 0x6d, 0x61 } };

// {87BCFA9C-1BD0-4d1c-B07A-F0F2954CAB81}
static const GUID IID_IVMGameMsg = 
{ 0x87bcfa9c, 0x1bd0, 0x4d1c, { 0xb0, 0x7a, 0xf0, 0xf2, 0x95, 0x4c, 0xab, 0x81 } };

// {10829C73-33E3-423b-BFBB-1592D483D9F4}
static const GUID IID_IVMGlobal = 
{ 0x10829c73, 0x33e3, 0x423b, { 0xbf, 0xbb, 0x15, 0x92, 0xd4, 0x83, 0xd9, 0xf4 } };

// {EF95E861-D06A-4ba5-88B2-B00E73A8131E}
static const GUID IID_IVMDebugJabFile = 
{ 0xef95e861, 0xd06a, 0x4ba5, { 0x88, 0xb2, 0xb0, 0xe, 0x73, 0xa8, 0x13, 0x1e } };

// {F181CBFB-6FD4-4e3c-8338-C90C7E700E39}
static const GUID IID_IVMPeekLock = 
{ 0xf181cbfb, 0x6fd4, 0x4e3c, { 0x83, 0x38, 0xc9, 0xc, 0x7e, 0x70, 0xe, 0x39 } };

// {6A481193-2EDF-4468-AF9F-DDFBEB3A1976}
static const GUID IID_IVMDebugMode = 
{ 0x6a481193, 0x2edf, 0x4468, { 0xaf, 0x9f, 0xdd, 0xfb, 0xeb, 0x3a, 0x19, 0x76 } };

// {1B78EBFF-C05F-4298-BB76-8C3517E9C91B}
static const GUID IID_IErrorMsgBox = 
{ 0x1b78ebff, 0xc05f, 0x4298, { 0xbb, 0x76, 0x8c, 0x35, 0x17, 0xe9, 0xc9, 0x1b } };

// {C32436B0-82EA-481e-B434-E06B59D6432F}
static const GUID IID_IVMDebugPage2 = 
{ 0xc32436b0, 0x82ea, 0x481e, { 0xb4, 0x34, 0xe0, 0x6b, 0x59, 0xd6, 0x43, 0x2f } };

// {D2DF4752-B4B8-467f-9E4B-3AB9C391DFFF}
static const GUID IID_IVMJITCompiler = 
{ 0xd2df4752, 0xb4b8, 0x467f, { 0x9e, 0x4b, 0x3a, 0xb9, 0xc3, 0x91, 0xdf, 0xff } };

// {22110FAF-901B-46d6-9A8B-FB12A3FFAD4E}
static const GUID IID_IOutputIDE = 
{ 0x22110faf, 0x901b, 0x46d6, { 0x9a, 0x8b, 0xfb, 0x12, 0xa3, 0xff, 0xad, 0x4e } };

#endif // __GJAFFAVM_H__