///////////////////////////////////////////////////////////////////////////////
//  IALDFileManager.h
//  Coder.Yudai Senoo :-)

#ifndef __IALDFILEMANAGER_H__
#define __IALDFILEMANAGER_H__

#include "IInterface.h"

interface IALDFile;
interface IALDFileManager : public IInterface {
	virtual IALDFile*	Get(int nType) = 0;		// IALDFile�̎擾
	/*
		0 LINK_CG		(GA.ALD)
		1 LINK_WAVE		(WA.ALD)
		2 LINK_DATA		(DA.ALD)
		3 LINK_RES		(RA.ALD)
		4 LINK_BGM		(BA.ALD)
	*/
};

#endif // __IALDFILEMANAGER_H__

