///////////////////////////////////////////////////////////////////////////////
//  IJoypadQuake.h
//  Coder.Yudai Senoo :-)

#ifndef __IJOYPADQUAKE_H__
#define __IJOYPADQUAKE_H__

#include <objbase.h>

interface IJoypadQuake {
	virtual void	Set(int nNum, int nType, int nMagnitude) = 0;
	// nType(0) = ConstantForce
	// nMagnitude�͈̔͂�0�`10000
};

#endif // __IJOYPADQUAKE_H__

