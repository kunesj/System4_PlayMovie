///////////////////////////////////////////////////////////////////////////////
//  IMainSystem.h
//  Coder.Yudai Senoo :-)

#ifndef __IMAINSYSTEM_H__
#define __IMAINSYSTEM_H__

#include <objbase.h>

interface IMainSystem {
	virtual void*	GetInterface(const GUID* pGuid) = 0;
};

#endif // __IMAINSYSTEM_H__

