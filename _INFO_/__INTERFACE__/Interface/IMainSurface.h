///////////////////////////////////////////////////////////////////////////////
//  IMainSurface.h
//  Coder.Yudai Senoo :-)

#ifndef __IMAINSURFACE_H__
#define __IMAINSURFACE_H__

#include "ISurface.h"

interface IMainSurface : public ISurface {
	virtual void	Update(int nX, int nY, int nWidth, int nHeight) = 0;	// 画面の更新
};

#endif // __IMAINSURFACE_H__

