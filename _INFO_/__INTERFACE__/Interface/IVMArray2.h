///////////////////////////////////////////////////////////////////////////////
//  IVMArray2.h
//  Coder.Yudai Senoo :-)

#ifndef __IVMARRAY2_H__
#define __IVMARRAY2_H__

#include "IVMArray.h"

interface IMemory;
interface IVMArray2 : public IVMArray {
	virtual bool	Alloc(int* pnSize, int nNumofSize) = 0;			// 確保
	virtual bool	Realloc(int* pnSize, int nNumofSize) = 0;		// 再確保
	virtual bool	Free(void) = 0;									// 解放

	// セーブ
	virtual IMemory*	Save(void) = 0;

	// ロード
	virtual bool		Load(void* pvData, int nSize) = 0;
};

/*
	Alloc/ReallocのpnSizeには各次元のサイズが入ったint配列への
	ポインタを渡して下さい。nNumofSizeには次元数を入れて下さい
	
	(例)
	int anSize[2] = { 10, 20 };
	IVMArray2:Alloc(anSize, 2);

*/

#endif // __IVMARRAY2_H__

