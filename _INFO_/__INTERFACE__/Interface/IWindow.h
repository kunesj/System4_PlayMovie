///////////////////////////////////////////////////////////////////////////////
//  IWindow.h
//  Coder.Yudai Senoo :-)

#ifndef __IWINDOW_H__
#define __IWINDOW_H__

#include <objbase.h>

interface IRect;

interface IWindow {
	virtual void*	GetHWND(void) = 0;		// ウィンドウハンドルの取得
	
	virtual IRect*	GetClientRect(void) = 0;
};

#endif // __IWINDOW_H__

