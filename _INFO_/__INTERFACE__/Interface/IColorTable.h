///////////////////////////////////////////////////////////////////////////////
//  IColorTable.h
//  Coder.Yudai Senoo :-)

#ifndef __ICOLORTABLE_H__
#define __ICOLORTABLE_H__

#include "IInterface.h"

interface IColorTable : public IInterface {
	virtual int		GetNumof() = 0;		// 個数の取得
	
	virtual bool	Set(int nIndex, int nR, int nG, int nB) = 0;		// 設定
	virtual bool	Get(int nIndex, int* pnR, int* pnG, int* pnB) = 0;	// 取得
};

#endif // __ICOLORTABLE_H__

