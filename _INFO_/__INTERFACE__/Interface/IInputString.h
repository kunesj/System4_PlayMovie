///////////////////////////////////////////////////////////////////////////////
//  IInputString.h
//  Coder.Yudai Senoo :-)

#ifndef __IINPUTSTRING_H__
#define __IINPUTSTRING_H__

#include <objbase.h>

interface IInputString {
	virtual void	SetFont(int nSize, char* pszName, int nWeight) = 0;		// フォントの設定
	virtual void	SetPos(int nX, int nY) = 0;		// コンポジションウィンドウの位置設定

	virtual void	Begin(void) = 0;				// 入力モードの開始
	virtual void	End(void) = 0;					// 入力モードの終了

	virtual void	OpenIME(void) = 0;
	virtual void	CloseIME(void) = 0;
	
	virtual char*	GetResultString(void) = 0;		// 確定文字列の取得
	virtual void	ClearResultString(void) = 0;	// 確定文字列バッファのクリア
};

#endif // __IINPUTSTRING_H__

