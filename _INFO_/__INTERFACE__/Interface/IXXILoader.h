///////////////////////////////////////////////////////////////////////////////
//  IXXILoader.h
//  Coder.Yudai Senoo :-)

#ifndef __IXXILOADER_H__
#define __IXXILOADER_H__

#include "IInterface.h"

interface IXXILoader : public IInterface {
	virtual bool	Load(char* pszFileName) = 0;		// ファイルの読み込み
	
	// 取得
	virtual int				GetVersion(void) = 0;
	virtual int				GetNumofLink(void) = 0;
	virtual int				GetNumofDWORD(void) = 0;
	virtual int				GetNumofString(void) = 0;
	
	virtual unsigned long	GetDWORD(int nLinkNum, int nIndex) = 0;
	virtual char*			GetString(int nLinkNum, int nIndex) = 0;
	virtual unsigned long	GetDataDWORD(char* pszKey) = 0;
	virtual char*			GetDataString(char* pszKey) = 0;
};

#endif // __IXXILOADER_H__

