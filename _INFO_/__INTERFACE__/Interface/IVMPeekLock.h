///////////////////////////////////////////////////////////////////////////////
//  IVMPeekLock.h
//  Coder.TYA-PA-

#ifndef __IVMPEEKLOCK_H__
#define __IVMPEEKLOCK_H__

#include <objbase.h>

interface IVMPeekLock {
	virtual int Lock(void) = 0;		// 戻り値、ロック回数
	virtual int Unlock(void) = 0;	// 戻り値、ロック回数
};

#endif	// __IVMPEEKLOCK_H__

