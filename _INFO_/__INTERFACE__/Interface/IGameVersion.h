///////////////////////////////////////////////////////////////////////////////
//  IGameVersion.h
//  Coder.Yudai Senoo :-)

#ifndef __IGAMEVERSION_H__
#define __IGAMEVERSION_H__

#include <objbase.h>

interface IGameVersion {
	virtual void	Set(int nVersion) = 0;		// �ݒ�
	virtual int		Get(void) = 0;				// �擾
};

#endif // __IGAMEVERSION_H__

