///////////////////////////////////////////////////////////////////////////////
//  ISurface.h
//  Coder.Yudai Senoo :-)

#ifndef __ISURFACE_H__
#define __ISURFACE_H__

#include "IInterface.h"

interface ISurface : public IInterface {
	virtual void*	GetPixel(int nX, int nY) = 0;		// ピクセルへのポインタ取得
	virtual void*	GetAlpha(int nX, int nY) = 0;		// αピクセルへのポインタ取得
	
	virtual int		GetWidth(void) = 0;					// 幅取得
	virtual int		GetHeight(void) = 0;				// 高さ取得
	virtual int		GetBpp(void) = 0;					// ｂｐｐ取得

	virtual int		GetPitch(void) = 0;					// ピクセルの１行バイト数取得
	virtual int		GetAlphaPitch(void) = 0;			// αピクセルの１行バイト数取得

	virtual bool	ExistPixel(void) = 0;				// ピクセルマップが存在するか調べる
	virtual bool	ExistAlpha(void) = 0;				// αマップが存在するか調べる
};

#endif // __ISURFACE_H__

