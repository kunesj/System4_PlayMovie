///////////////////////////////////////////////////////////////////////////////
//  IVM.h
//  Coder.Yudai Senoo :-)

#ifndef __IVM_H__
#define __IVM_H__

#include "IInterface.h"

interface IMainSystem;

interface IVM : public IInterface {
	virtual bool	Init(IMainSystem* pIMainSystem) = 0;	// 初期化
	virtual bool	Run(void) = 0;							// 実行

	virtual void*		GetInterface(const GUID* pGuid) = 0;
	virtual IInterface*	CreateInterface(const GUID* pGuid) = 0;
};

#endif // __IVM_H__

