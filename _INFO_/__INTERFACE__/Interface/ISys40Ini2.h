///////////////////////////////////////////////////////////////////////////////
//  ISys40Ini2.h
//  Coder.Yudai Senoo :-)

#ifndef __ISYS40INI2_H__
#define __ISYS40INI2_H__

#include "ISys40Ini.h"

interface ISys40Ini2 : public ISys40Ini {
	virtual char*	GetSaveFolderName(void) = 0;	// セーブフォルダ名の取得
};

#endif // __ISYS40INI2_H__

