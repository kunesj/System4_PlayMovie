///////////////////////////////////////////////////////////////////////////////
//  IGraph2.h
//  Coder.Yudai Senoo :-)

#ifndef __IGRAPH2_H__
#define __IGRAPH2_H__

#include "IGraph.h"

interface IGraph2 : public IGraph {
	virtual void	BlendDA_DAxInvSA(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;
	virtual void	MaxDA_DAxSA(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;
	virtual void	BlendDA_DAxSA(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;
	virtual void	SubDA_DAxSA(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;

	virtual void	BlendMultiplyAlpha(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nAlpha) = 0;

	virtual void	SpriteCopyAMap(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nColorKey) = 0;
	virtual void	FillAMapOverBorder(ISurface* pSurface, int nX, int nY, int nWidth, int nHeight, int nAlpha, int nBorder) = 0;
	virtual void	FillAMapUnderBorder(ISurface* pSurface, int nX, int nY, int nWidth, int nHeight, int nAlpha, int nBorder) = 0;

	virtual void	FillScreen(ISurface* pSurface, int nX, int nY, int nWidth, int nHeight, int nR, int nG, int nB) = 0;
	virtual void	FillMultiply(ISurface* pSurface, int nX, int nY, int nWidth, int nHeight, int nR, int nG, int nB) = 0;

	virtual void	CopySprite(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nR, int nG, int nB) = 0;

	virtual void	CopyStretchInterp(ISurface* pSurface, float fDx, float fDy, float fDWidth, float fDHeight, ISurface* pSrcSurface, float fSx, float fSy, float fSWidth, float fSHeight) = 0;
	virtual void	CopyStretchAMap(ISurface* pSurface, int nDx, int nDy, int nDWidth, int nDHeight, ISurface* pSrcSurface, float fSx, float fSy, float fSWidth, float fSHeight) = 0;
	virtual void	CopyStretchAMapInterp(ISurface* pSurface, float fDx, float fDy, float fDWidth, float fDHeight, ISurface* pSrcSurface, float fSx, float fSy, float fSWidth, float fSHeight) = 0;

	virtual void	CopyReduce(ISurface* pDestSurface, int nDx, int nDy, int nDWidth, int nDHeight, ISurface* pSrcSurface, int nSx, int nSy, int nSWidth, int nSHeight) = 0;
	virtual void	CopyReduceAMap(ISurface* pDestSurface, int nDx, int nDy, int nDWidth, int nDHeight, ISurface* pSrcSurface, int nSx, int nSy, int nSWidth, int nSHeight) = 0;

	virtual void	FillAMapGradationLR(ISurface* pSurface, int nX, int nY, int nWidth, int nHeight, int nLeftA, int nRightA) = 0;
	virtual void	FillAMapGradationUD(ISurface* pSurface, int nX, int nY, int nWidth, int nHeight, int nUpA, int nDownA) = 0;

	virtual void	CopyRotZoom(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, float fRad, float fMag) = 0;
	virtual void	CopyRotZoomUseAMap(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, float fRad, float fMag) = 0;

	virtual void	CopyRotateY(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag) = 0;
	virtual void	CopyRotateYUseAMap(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag) = 0;
	virtual void	CopyRotateYFixL(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag) = 0;
	virtual void	CopyRotateYFixR(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag) = 0;
	virtual void	CopyRotateYFixLUseAMap(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag) = 0;
	virtual void	CopyRotateYFixRUseAMap(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag) = 0;
	virtual void	CopyRotateX(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag) = 0;
	virtual void	CopyRotateXUseAMap(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag) = 0;
	virtual void	CopyRotateXFixU(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag) = 0;
	virtual void	CopyRotateXFixD(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag) = 0;
	virtual void	CopyRotateXFixUUseAMap(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag) = 0;
	virtual void	CopyRotateXFixDUseAMap(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag) = 0;

	virtual void	CopyReverseLR(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;
	virtual void	CopyReverseUD(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;
	virtual void	CopyReverseAMapLR(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;
	virtual void	CopyReverseAMapUD(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;

	virtual void	CopyUseAMapUnder(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nAlpha) = 0;
	virtual void	CopyUseAMapBorder(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nAlpha) = 0;
	virtual void	CopyAMapMax(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;
	virtual void	CopyAMapMin(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;

	virtual void	CopyWidthBlur(ISurface* pIDestSurface, int nDx, int nDy, ISurface* pISrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nBlur) = 0;
	virtual void	CopyHeightBlur(ISurface* pIDestSurface, int nDx, int nDy, ISurface* pISrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nBlur) = 0;

	virtual void	CopyColorReverse(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;
	virtual void	CopyAMapReverse(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;
};

#endif // __IGRAPH2_H__

