///////////////////////////////////////////////////////////////////////////////
//  IReadFile.h
//  Coder.Yudai Senoo :-)

#ifndef __IREADFILE_H__
#define __IREADFILE_H__

#include "IInterface.h"

interface IString;

interface IReadFile : public IInterface {
	virtual bool	Load(char* pszFileName) = 0;

	// データ読み込み
	virtual bool	Read(void* pvData, int nSize) = 0;
	virtual bool	ReadInt(int* pnData) = 0;
	virtual bool	ReadBYTE(unsigned char* pucData) = 0;
	virtual bool	ReadWORD(unsigned short* pusData) = 0;
	virtual bool	ReadDWORD(unsigned int* punData) = 0;
	virtual bool	ReadString(IString* pIString) = 0;

	// データ位置指定読み込み
	virtual bool	ReadPos(int nPos, void* pvData, int nSize) = 0;
	virtual bool	ReadPosInt(int nPos, int* pnData) = 0;
	virtual bool	ReadPosBYTE(int nPos, unsigned char* pucData) = 0;
	virtual bool	ReadPosWORD(int nPos, unsigned short* pusData) = 0;
	virtual bool	ReadPosDWORD(int nPos, unsigned int* punData) = 0;
	virtual bool	ReadPosString(int nPos, IString* pIString) = 0;

	// 情報
	virtual int		GetSize(void) = 0;
	virtual int		GetReadPos(void) = 0;
	virtual bool	SetReadPos(int nPos) = 0;
	virtual bool	SkipReadPos(int nSkip) = 0;

};

#endif // __IREADFILE_H__

