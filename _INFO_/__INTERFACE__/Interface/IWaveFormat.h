#ifndef __IWAVEFORMAT_H__
#define __IWAVEFORMAT_H__
//kazushi nagaoka

#include "IInterface.h"
#include <objbase.h>

interface IWaveFormat : public IInterface
{
	virtual unsigned int	GetFormatTag(void) = 0;
	virtual unsigned int	GetChannels(void) = 0;
	virtual unsigned int	GetSamplesPerSec(void) = 0;
	virtual unsigned int	GetAvgBytesPerSec(void) = 0;
	virtual unsigned int	GetBlockAlign(void) = 0;
	virtual unsigned int	GetBitsPerSample(void) = 0;

	virtual void			SetFormatTag(unsigned int unFormatTag) = 0;
	virtual void			SetChannels(unsigned int unChannels) = 0;
	virtual void			SetSamplesPerSec(unsigned int unSamplesPerSec) = 0;
	virtual void			SetAvgBytesPerSec(unsigned int unAvgBytesPerSec) = 0;
	virtual void			SetBlockAlign(unsigned int unBlockAlign) = 0;
	virtual void			SetBitsPerSample(unsigned int unBitsPerSample) = 0;
};

#endif // __IWAVEFORMAT_H__
