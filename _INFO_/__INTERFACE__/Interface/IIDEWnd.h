///////////////////////////////////////////////////////////////////////////////
//  IIDEWnd.h
//  Coder.Yudai Senoo :-)

#ifndef __IIDEWND_H__
#define __IIDEWND_H__

#include <objbase.h>

interface IIDEWnd {
	virtual void*	GetHWND(void) = 0;		// ウィンドウハンドルの取得
};

#endif // __IIDEWND_H__

