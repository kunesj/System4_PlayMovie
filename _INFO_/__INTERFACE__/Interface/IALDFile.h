///////////////////////////////////////////////////////////////////////////////
//  IALDFile.h
//  Coder.Yudai Senoo :-)

#ifndef __IALDFILE_H__
#define __IALDFILE_H__

#include "ILinkFile.h"

interface IMemory;

interface IALDFile : public ILinkFile {
	virtual bool	IsLink(int nNum) = 0;
	virtual bool	IsData(int nNum) = 0;

	virtual int		GetNumof(void) = 0;				// リンクの個数を取得（正確な数値ではない）
	virtual void*	GetFILETIME(int nNum) = 0;		// FILETIME構造体へのポインタを取得
	virtual char*	GetFileName(int nNum) = 0;		// ファイル名の取得

	virtual char*	GetALDFileName(int nDisk) = 0;	// ＡＬＤファイル名の取得（nDisk : 0〜）
};

#endif // __IALDFILE_H__

