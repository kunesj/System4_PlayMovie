///////////////////////////////////////////////////////////////////////////////
//  GGrEffect.h
//  Coder.Yudai Senoo :-)

#ifndef __GGREFFECT_H__
#define __GGREFFECT_H__

#include <objbase.h>

// {D6433341-4DB3-11d4-BEB2-00C0F6B0E9BE}
static const GUID IID_IGrEffect = 
{ 0xd6433341, 0x4db3, 0x11d4, { 0xbe, 0xb2, 0x0, 0xc0, 0xf6, 0xb0, 0xe9, 0xbe } };

#endif // __GGREFFECT_H__

