///////////////////////////////////////////////////////////////////////////////
//  IShutdown.h
//  Coder.Yudai Senoo :-)

#ifndef __ISHUTDOWN_H__
#define __ISHUTDOWN_H__

#include <objbase.h>

interface IShutdown {
	virtual void	Shutdown(void) = 0;		// アプリを終了させる
};

#endif // __ISHUTDOWN_H__

