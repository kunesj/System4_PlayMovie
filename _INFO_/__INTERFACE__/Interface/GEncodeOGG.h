///////////////////////////////////////////////////////////////////////////////
//  GEncodeOGG.h
//  Coder.Yudai Senoo :-)

#ifndef __GENCODEOGG_H__
#define __GENCODEOGG_H__

#include <objbase.h>

// {E83CA7F1-5F85-4c96-9023-B8B0650A3D23}
static const GUID IID_IEncodeOGG = 
{ 0xe83ca7f1, 0x5f85, 0x4c96, { 0x90, 0x23, 0xb8, 0xb0, 0x65, 0xa, 0x3d, 0x23 } };

#endif // __GENCODEOGG_H__

