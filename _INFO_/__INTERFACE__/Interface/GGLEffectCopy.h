///////////////////////////////////////////////////////////////////////////////
//  GGLEffectCopy.h
//  Coder.Yudai Senoo :-)

#ifndef __GGLEFFECTCOPY_H__
#define __GGLEFFECTCOPY_H__

#include <objbase.h>

// {F9B66380-84D4-11d4-BEB2-00C0F6B0E9BE}
static const GUID IID_IGLEffectCopy =
{ 0xf9b66380, 0x84d4, 0x11d4, { 0xbe, 0xb2, 0x0, 0xc0, 0xf6, 0xb0, 0xe9, 0xbe } };

// {4BB94901-8F22-47ad-B543-84880E71FEA7}
static const GUID IID_IGLEffectCopyQuake = 
{ 0x4bb94901, 0x8f22, 0x47ad, { 0xb5, 0x43, 0x84, 0x88, 0xe, 0x71, 0xfe, 0xa7 } };

#endif // __GGLEFFECTCOPY_H__

