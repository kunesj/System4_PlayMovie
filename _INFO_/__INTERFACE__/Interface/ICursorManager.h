///////////////////////////////////////////////////////////////////////////////
//  ICursorManager.h
//  Coder.Yudai Senoo :-)

#ifndef __ICURSORMANAGER_H__
#define __ICURSORMANAGER_H__

#include <objbase.h>

interface ICursorManager {
	virtual bool	Load(int nNum, void* pvData, int nSize) = 0;	// カーソルの読み込み
	virtual void	Set(int nNum) = 0;								// カーソルの設定
};

#endif // __ICURSORMANAGER_H__

