///////////////////////////////////////////////////////////////////////////////
//  IInterface.h
//  Coder.Yudai Senoo :-)

#ifndef __IINTERFACE_H__
#define __IINTERFACE_H__

#include <objbase.h>

interface IInterface {
	virtual int		AddRef(void) = 0;
	virtual int		Release(void) = 0;					// ���
};

#endif // __IINTERFACE_H__

