///////////////////////////////////////////////////////////////////////////////
//  IJaffaDebug.h
//  Coder.Yudai Senoo :-)

#ifndef __IJAFFADEBUG_H__
#define __IJAFFADEBUG_H__

#include <objbase.h>

interface IJaffaDebug {
	virtual void*	GetHWND(void) = 0;

	virtual void*	GetInterface(const GUID* pGuid) = 0;
};

#endif // __IJAFFADEBUG_H__

