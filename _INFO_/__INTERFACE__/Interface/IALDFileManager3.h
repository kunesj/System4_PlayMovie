///////////////////////////////////////////////////////////////////////////////
//  IALDFileManager3.h
//  Coder.Yudai Senoo :-)

#ifndef __IALDFILEMANAGER3_H__
#define __IALDFILEMANAGER3_H__

#include "IALDFileManager2.h"

interface IALDFile2;
interface IALDFileManager3 : public IALDFileManager2 {
	virtual IALDFile2*	GetIALDFile2(int nType) = 0;	// IALDFile2�̎擾
	/*
		0 LINK_CG		(GA.ALD)
		1 LINK_WAVE		(WA.ALD)
		2 LINK_DATA		(DA.ALD)
		3 LINK_RES		(RA.ALD)
		4 LINK_BGM		(BA.ALD)
	*/
};

#endif // __IALDFILEMANAGER3_H__

