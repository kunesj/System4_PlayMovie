///////////////////////////////////////////////////////////////////////////////
//  IDecodeCG.h
//  Coder.Yudai Senoo :-)

#ifndef __IDECODECG_H__
#define __IDECODECG_H__

#include "IInterface.h"

interface ICGSurface;

interface IDecodeCG : public IInterface {
	virtual ICGSurface*		Decode(void* pvData, int nSize) = 0;
};

#endif // __IDECODECG_H__

