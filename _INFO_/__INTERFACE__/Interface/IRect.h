///////////////////////////////////////////////////////////////////////////////
//  IRect.h
//  Coder.Yudai Senoo :-)

#ifndef __IRECT_H__
#define __IRECT_H__

#include <objbase.h>

interface IRect {
	// �ݒ�
	virtual void	SetX(int nX) = 0;
	virtual void	SetY(int nY) = 0;
	virtual void	SetWidth(int nWidth) = 0;
	virtual void	SetHeight(int nHeight) = 0;
	
	// �擾
	virtual int		GetX(void) = 0;
	virtual int		GetY(void) = 0;
	virtual int		GetWidth(void) = 0;
	virtual int		GetHeight(void) = 0;
};

#endif // __IRECT_H__

