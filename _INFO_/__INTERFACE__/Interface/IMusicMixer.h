///////////////////////////////////////////////////////////////////////////////
//  IMusicMixer.h
//  Coder.Yudai Senoo :-)

#ifndef __IMUSICMIXER_H__
#define __IMUSICMIXER_H__

#include <objbase.h>

interface IMusicMixer {
	virtual bool	Fade(int nTime, int nVolume, bool bStop) = 0;	// 指定ボリュームへフェード
	virtual bool	StopFade(void) = 0;								// フェードの停止
	virtual bool	IsFade(void) = 0;								// フェード中かどうか
};

#endif // __IMUSICMIXER_H__

