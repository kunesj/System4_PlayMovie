///////////////////////////////////////////////////////////////////////////////
//  IVMArray.h
//  Coder.Yudai Senoo :-)

#ifndef __IVMARRAY_H__
#define __IVMARRAY_H__

#include <objbase.h>

interface IString;
interface IVMStruct;

interface IVMArray {
	virtual int		GetType(void) = 0;			// 型
	virtual char*	GetStructName(void) = 0;	// 構造体型だった場合の名前
	
	virtual int		GetNumof(void) = 0;		// 個数
	virtual int		GetDim(void) = 0;		// 次元

	// 要素取得
	virtual void*	GetData(int nIndex)=0;
	//GetTypeで返ってくる値と、GetDataで取得できるポインタ種の対応表
	//	OBJTYPE_INT		int*
	//	OBJTYPE_FLOAT	float*
	//	OBJTYPE_STRING	IString*
	//	OBJTYPE_STRUCT	IVMStruct
	//	OBJTYPE_A*****	IVMArray

	//要素設定
	virtual bool	SetData(int nIndex, void* pvData) = 0;
	//GetTypeで返ってくる値と、SetDataで設定する内容の対応表
	//	OBJTYPE_INT		int*
	//	OBJTYPE_FLOAT	float*
	//	OBJTYPE_STRING	char*
	//	OBJTYPE_STRUCT	不可
	//	OBJTYPE_A*****	不可

};

#endif // __IVMARRAY_H__

