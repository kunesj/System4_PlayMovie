#ifndef __ISOUNDDATA_H__
#define __ISOUNDDATA_H__
//ISoundData.h
//kazushi

#include "IInterface.h"
#include <objbase.h>

interface IWaveFormat;

interface ISoundData : public IInterface
{
	virtual bool			Open(char* pszFileName, unsigned int unPos, unsigned int unSize) = 0;
	virtual bool			Close(void) = 0;

	virtual bool			Get(void* pvData, unsigned int unSize, unsigned int* punDoneSize) = 0;
	virtual bool			Seek(unsigned int unPos) = 0;

	virtual bool			IsOpen(void) = 0;
	virtual	bool			GetWaveFormat(IWaveFormat* pIWaveFormat) = 0;

	virtual unsigned int	GetReadPos(void) = 0;
	virtual unsigned int	GetDataSize(void) = 0;
};

//位置・サイズはすべて(展開後)バイト数で設定・取得

#endif // __ISOUNDDATA_H__
