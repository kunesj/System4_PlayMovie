///////////////////////////////////////////////////////////////////////////////
//  G4xMusic.h
//  Coder.TYA-PA- :-)

#ifndef __G4XMUSIC_H__
#define __G4XMUSIC_H__

// {F4300AB5-4EAC-49e2-B4E3-0BCB1F792D67}
static const GUID IID_IS4xMusic = 
{ 0xf4300ab5, 0x4eac, 0x49e2, { 0xb4, 0xe3, 0xb, 0xcb, 0x1f, 0x79, 0x2d, 0x67 } };

// {1926F938-C1AB-46b7-B38E-DCA05376CC2A}
static const GUID IID_IMusicWaitInfo = 
{ 0x1926f938, 0xc1ab, 0x46b7, { 0xb3, 0x8e, 0xdc, 0xa0, 0x53, 0x76, 0xcc, 0x2a } };

#endif // __G3XMUSIC_H__
