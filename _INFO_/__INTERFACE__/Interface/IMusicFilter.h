///////////////////////////////////////////////////////////////////////////////
//  IMusicFilter.h
//  Coder.Yudai Senoo :-)

#ifndef __IMUSICFILTER_H__
#define __IMUSICFILTER_H__

#include <objbase.h>

interface IMusicFilter {
	virtual bool	ReverseLR(void) = 0;		// �k�q���]
};

#endif // __IMUSICFILTER_H__

