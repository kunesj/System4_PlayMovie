///////////////////////////////////////////////////////////////////////////////
//  IJoypad2.h
//  Coder.Yudai Senoo :-)

#ifndef __IJOYPAD2_H__
#define __IJOYPAD2_H__

#include "IJoypad.h"

interface IJoypad2 : public IJoypad {
	// アナログスティックの状態取得
	virtual void	GetAnalogStickStatus(int nNum, int nType, float* pfDegree, float* pfPower) = 0;
	/*
		nNum		: ジョイパッド番号（0〜）
		nType		: 0=左スティック, 1=右スティック
		pfDegree	: 角度（上を０度とする時計回り）（0.0<=角度<360.0）
		pfPower		: 倒している力（0.0<=力<=1.0）
	*/
	
	// デジタルスティックの状態取得
	virtual void	GetDigitalStickStatus(int nNum, int nType, bool* pbLeft, bool* pbRight, bool* pbUp, bool* pbDown) = 0;
	/*
		nNum		: ジョイパッド番号（0〜）
		nType		: 0=左十字キー
		pbLeft		: 押されていればtrue,押されていなければfalse
		pbRight
		pbUp
		pbDown
	*/
	
};

#endif // __IJOYPAD2_H__

