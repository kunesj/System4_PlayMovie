///////////////////////////////////////////////////////////////////////////////
//  ECmdList42.h
//  Coder.Yudai Senoo :-)

#ifndef __ECMDLIST42_H__
#define __ECMDLIST42_H__

enum ECmd {
	CMD_NONE = -1,

	CMD_PUSH,			// 次の４バイトデータをプッシュ(0)
	CMD_POP,			// (1)
	CMD_REF,			// ページとインデックスをポップして、データをプッシュ(2)
	CMD_REFREF,			// ページとインデックスをポップして、ページとインデックスをプッシュ(3)
	CMD_PUSHGLOBALPAGE,	// (4)
	CMD_PUSHLOCALPAGE,	// (5)

	// 単項演算子
	CMD_INV,		// '-'(6)
	CMD_NOT,		// '!'(7)
	CMD_COMPL,		// '~'(8)

	// 二項演算子
	CMD_ADD,		// '+'(9)
	CMD_SUB,		// '-'(10)
	CMD_MUL,		// '*'(11)
	CMD_DIV,		// '/'(12)
	CMD_MOD,		// '%'(13)
	CMD_AND,		// '&'(14)
	CMD_OR,			// '|'(15)
	CMD_XOR,		// '^'(16)
	CMD_LSHIFT,		// '<<'(17)
	CMD_RSHIFT,		// '>>'(18)
	CMD_LT,			// '<'(19)
	CMD_GT,			// '>'(20)
	CMD_LTE,		// '<='(21)
	CMD_GTE,		// '>='(22)
	CMD_NOTE,		// '!='(23)
	CMD_EQUALE,		// '=='(24)

	// 代入演算子
	CMD_ASSIGN,		// '='(25)
	CMD_PLUSA,		// '+='(26)
	CMD_MINUSA,		// '-='(27)
	CMD_MULA,		// '*='(28)
	CMD_DIVA,		// '/='(29)
	CMD_MODA,		// '%='(30)
	CMD_ANDA,		// '&='(31)
	CMD_ORA,		// '|='(32)
	CMD_XORA,		// '^='(33)
	CMD_LSHIFTA,	// '<<='(34)
	CMD_RSHIFTA,	// '>>='(35)

	CMD_F_ASSIGN,	// '='(36)
	CMD_F_PLUSA,	// '+='(37)
	CMD_F_MINUSA,	// '-='(38)
	CMD_F_MULA,		// '*='(39)
	CMD_F_DIVA,		// '/='(40)

	// スタック操作
	CMD_DUP2,		// ２ワードコピー(41)
	CMD_DUP_X2,		// (42)

	// 比較演算
	CMD_CMP,		// (43)

	// 比較ジャンプ
	CMD_JUMP,		// 無条件ジャンプ(44)
	CMD_IFZ,		// ０ならジャンプ(45)
	CMD_IFNZ,		// ０以外ならジャンプ(46)

	CMD_RETURN,		// (47)
	CMD_CALLFUNC,	// (48)

	//
	CMD_INC,		// (49)
	CMD_DEC,		// (50)

	// 型変換
	CMD_FTOI,		// (51)
	CMD_ITOF,		// (52)

	// float演算
	CMD_F_INV,		// '-'(53)

	CMD_F_ADD,		// '+'(54)
	CMD_F_SUB,		// '-'(55)
	CMD_F_MUL,		// '*'(56)
	CMD_F_DIV,		// '/'(57)
	CMD_F_LT,		// '<'(58)
	CMD_F_GT,		// '>'(59)
	CMD_F_LTE,		// '<='(60)
	CMD_F_GTE,		// '>='(61)
	CMD_F_NOTE,		// '!='(62)
	CMD_F_EQUALE,	// '=='(63)

	CMD_F_PUSH,		// (64)

	// 文字列演算
	CMD_S_PUSH,		// (65)
	CMD_S_POP,		// (66)
	CMD_S_ADD,		// (67)
	CMD_S_ASSIGN,	// (68)
	CMD_S_PLUSA,	// (69)
	CMD_S_REF,		// (70)
	CMD_S_REFREF,	// (71)
	CMD_S_NOTE,		// '!='(72)
	CMD_S_EQUALE,	// '=='(73)

	// サーフェスコマンド
	CMD_SF_CREATE,			// (74)
	CMD_SF_CREATEPIXEL,		// (75)
	CMD_SF_CREATEALPHA,		// (76)

	// 構造体オブジェクト演算,
	CMD_SR_POP,			// (77)
	CMD_SR_ASSIGN,		// (78)
	CMD_SR_REF,			// (79)
	CMD_SR_REFREF,		// (80)

	// 配列コマンド
	CMD_A_ALLOC,		// (81)
	CMD_A_REALLOC,		// (82)
	CMD_A_FREE,			// (83)
	CMD_A_NUMOF,		// (84)
	CMD_A_COPY,			// (85)
	CMD_A_FILL,			// (86)

	// 文字コマンド
	CMD_C_REF,			// (87)
	CMD_C_ASSIGN,		// (88)

	// メッセージ表示
	CMD_MSG,			// (89)

	// ＨＬＬ
	CMD_CALLHLL,		// (90)

	CMD_PUSHSTRUCTPAGE,	// (91)
	CMD_CALLMETHOD,		// (92)

	// 短縮命令
	CMD_SH_GLOBALREF,	// (93)
	CMD_SH_LOCALREF,	// (94)

	CMD_SWITCH,			// (95)
	CMD_STRSWITCH,		// (96)

	CMD_FUNC,			// (97)
	CMD_EOF,			// (98)

	CMD_CALLSYS,		// (99)
	CMD_SJUMP,			// (100)
	CMD_CALLONJUMP,		// (101)

	CMD_SWAP,			// (102)

	CMD_SH_STRUCTREF,	// (103)

	// 文字列コマンド
	CMD_S_LENGTH,		// (104)
	CMD_S_LENGTHBYTE,	// (105)

	// intコマンド
	CMD_I_STRING,		// (106)

	// 関数呼び出し
	CMD_CALLFUNC2,		// (107)

	// スタック操作
	CMD_DUP2_X1,		// (108)

	// 参照オブジェクト代入
	CMD_R_ASSIGN,		// (109)

	// 関数型へ文字列代入
	CMD_FT_ASSIGNS,		// (110)

	CMD_ASSERT,			// (111)

	// 文字列演算
	CMD_S_LT,			// '<'(112)
	CMD_S_GT,			// '>'(113)
	CMD_S_LTE,			// '<='(114)
	CMD_S_GTE,			// '>='(115)

	// 文字列コマンド
	CMD_S_LENGTH2,		// (116)
	CMD_S_LENGTHBYTE2,	// (117)

	CMD_NEW,			// (118)
	CMD_DELETE,			// (119)
	CMD_CHECKUDO,		// (120)

	CMD_A_REF,			// (121)

	CMD_DUP,			// (122)
	CMD_DUP_U2,			// (123)

	CMD_SP_INC,			// (124)
	CMD_SP_DEC,			// (125)

	CMD_ENDFUNC,		// (126)

	CMD_R_EQUALE,		// '==='(127)
	CMD_R_NOTE,			// '!=='(128)

	CMD_SH_LOCALCREATE,	// (129)
	CMD_SH_LOCALDELETE,	// (130)

	CMD_STOI,			// (131)

	CMD_A_PUSHBACK,		// (132)
	CMD_A_POPBACK,		// (133)

	CMD_S_EMPTY,		// (134)
	CMD_A_EMPTY,		// (135)

	CMD_A_ERASE,		// (136)
	CMD_A_INSERT,		// (137)

	CMD_SH_LOCALINC,	// (138)
	CMD_SH_LOCALDEC,	// (139)
	CMD_SH_LOCALASSIGN,	// (140)

	CMD_ITOB,			// (141)

	CMD_S_FIND,			// (142)
	CMD_S_GETPART,		// (143)

	CMD_A_SORT,			// (144)

	CMD_S_PUSHBACK,		// (145)
	CMD_S_POPBACK,		// (146)

	CMD_FTOS,			// (147)

	CMD_S_MOD,			// (148)
	CMD_S_PLUSA2,		// (149)

	CMD_OBJSWAP,		// (150)

	// アセンブラ用
	CMD_LABEL = 1024,	// (1024)
	CMD_SLABEL,			// (1025)
	CMD_BR,				// (1026)
	CMD_NOP,			// (1027)
	CMD_LINEINFO,		// (1028)
	CMD_COMMENT,		// (1029)
	CMD_GOTOLABEL,		// (1030)
	CMD_GOTO,			// (1031)
};

#endif // __ECMDLIST42_H__

