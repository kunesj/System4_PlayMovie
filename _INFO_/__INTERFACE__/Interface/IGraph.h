///////////////////////////////////////////////////////////////////////////////
//  IGraph.h
//  Coder.Yudai Senoo :-)

#ifndef __IGRAPH_H__
#define __IGRAPH_H__

#include "IInterface.h"

interface ISurface;

interface IGraph : public IInterface {
	virtual bool	Init(void) = 0;					// 初期化

	virtual void	SetUseCPUEx(bool bUse) = 0;		// ＣＰＵ拡張命令を使用するかどうかのフラグ設定

	virtual void	Copy(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;
	virtual void	CopyAlphaMap(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;

	virtual void	CopyBright(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nRate) = 0;

	// α値参照ブレンド
	virtual void	Blend(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nAlpha) = 0;
	virtual void	BlendSrcBright(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nAlpha, int nRate) = 0;
	virtual void	BlendAddSatur(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;

	// αマップ参照ブレンド
	virtual void	BlendAlphaMap(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;													// DestPixel = Blend(DestPixel, SrcPixel, SrcAlphaMap)
	virtual void	BlendAlphaMapSrcOnly(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;											// DestPixel = Satur(DestPixel + Src x AlphaMap)
	virtual void	BlendAlphaMapColor(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nR, int nG, int nB) = 0;						// DestPixel = Blend(DestPixel, Color, SrcAlphaMap)
	virtual void	BlendAlphaMapColorAlpha(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nR, int nG, int nB, int nAlpha) = 0;	// DestPixel = Blend(DestPixel, Color, SrcAlphaMap x nAlpha)
	virtual void	BlendAlphaMapAlpha(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nAlpha) = 0;									// DestPixel = Blend(DestPixel, SrcPixel, SrcAlphaMap x nAlpha)
	virtual void	BlendAlphaMapBright(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nRate) = 0;									// DestPixel = Blend(DestPixel, SrcPixel x nRate, SrcAlphaMap)
	virtual void	BlendAlphaMapAlphaSrcBright(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nAlpha, int nRate) = 0;				// DestPixel = Blend(DestPixel, SrcPixel x nRate, SrcAlphaMap x nAlpha)

	virtual void	BlendUseAMapColor(ISurface* pDestSurface, int nDx, int nDy, int nWidth, int nHeight, ISurface* pAlphaSurface, int nAx, int nAy, int nR, int nG, int nB, int nAlpha) = 0;

	// フォトショップフィルタ
	virtual void	BlendScreen(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;	// フォトショップフィルタ：スクリーン
	virtual void	BlendMultiply(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;	// フォトショップフィルタ：乗算

	virtual void	BlendScreenAlpha(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight, int nAlpha) = 0;	// フォトショップフィルタ：スクリーンＸα

	// 塗りつぶし
	virtual void	Fill(ISurface* pSurface, int nX, int nY, int nWidth, int nHeight, int nR, int nG, int nB) = 0;
	virtual void	FillAlphaMap(ISurface* pSurface, int nX, int nY, int nWidth, int nHeight, int nAlpha) = 0;
	virtual void	FillAlphaColor(ISurface* pSurface, int nX, int nY, int nWidth, int nHeight, int nR, int nG, int nB, int nRate) = 0;

	virtual void	SaturDP_DPxSA(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;		// DestPixel = Satur(DestPixel + SrcAlpha)

	// α＝αＸα
	virtual void	ScreenDA_DAxSA(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;		// DestAlpha = Screen(DestAlpha, SrcAlpha)
	virtual void	AddDA_DAxSA(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;		// DestAlpha = Satur(DestAlpha + SrcAlpha)

	// 矩形効果
	virtual void	BrightDestOnly(ISurface* pSurface, int nX, int nY, int nWidth, int nHeight, int nRate) = 0;

	// テクスチャラップ
	virtual void	CopyTextureWrap(ISurface* pDestSurface, int nDx, int nDy, int nDWidth, int nDHeight, ISurface* pSrcSurface, int nSx, int nSy, int nSWidth, int nSHeight, int nU, int nV) = 0;
	virtual void	CopyTextureWrapAlpha(ISurface* pDestSurface, int nDx, int nDy, int nDWidth, int nDHeight, ISurface* pSrcSurface, int nSx, int nSy, int nSWidth, int nSHeight, int nU, int nV, int nAlpha) = 0;

	// 拡大縮小
	virtual void	CopyStretch(ISurface* pSurface, int nDx, int nDy, int nDWidth, int nDHeight, ISurface* pSrcSurface, float fSx, float fSy, float fSWidth, float fSHeight) = 0;
	virtual void	CopyStretchBlend(ISurface* pSurface, int nDx, int nDy, int nDWidth, int nDHeight, ISurface* pSrcSurface, float fSx, float fSy, float fSWidth, float fSHeight, int nAlpha) = 0;
	virtual void	CopyStretchBlendAlphaMap(ISurface* pSurface, int nDx, int nDy, int nDWidth, int nDHeight, ISurface* pSrcSurface, float fSx, float fSy, float fSWidth, float fSHeight) = 0;

};

#endif // __IGRAPH_H__

