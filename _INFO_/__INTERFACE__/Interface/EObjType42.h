///////////////////////////////////////////////////////////////////////////////
//  EObjType42.h
//  Coder.Yudai Senoo :-)

#ifndef __EOBJTYPE42_H__
#define __EOBJTYPE42_H__

#include "windows.h"

enum EObjType {
	OBJTYPE_ERROR	= -1,

	OBJTYPE_VOID,			// void							( 0)

	OBJTYPE_CHAR,			// string[]						( 1)

	OBJTYPE_V_INT,			// value int					( 2)
	OBJTYPE_V_FLOAT,		// value float					( 3)
	OBJTYPE_V_STRING,		// value string					( 4)
	OBJTYPE_V_STRUCT,		// value struct XXX				( 5)
	
	OBJTYPE_VA_INT,			// value array@int				( 6)
	OBJTYPE_VA_FLOAT,		// value array@float			( 7)
	OBJTYPE_VA_STRING,		// value array@string			( 8)
	OBJTYPE_VA_STRUCT,		// value array@struct XXX		( 9)

	OBJTYPE_INT,			// int							(10)
	OBJTYPE_FLOAT,			// float						(11)
	OBJTYPE_STRING,			// string						(12)
	OBJTYPE_STRUCT,			// struct XXX					(13)

	OBJTYPE_A_INT,			// array@int					(14)
	OBJTYPE_A_FLOAT,		// array@float					(15)
	OBJTYPE_A_STRING,		// array@string					(16)
	OBJTYPE_A_STRUCT,		// array@struct XXX				(17)

	OBJTYPE_R_INT,			// ref int						(18)
	OBJTYPE_R_FLOAT,		// ref float					(19)
	OBJTYPE_R_STRING,		// ref string					(20)
	OBJTYPE_R_STRUCT,		// ref struct XXX				(21)

	OBJTYPE_RA_INT,			// ref array@int				(22)
	OBJTYPE_RA_FLOAT,		// ref array@float				(23)
	OBJTYPE_RA_STRING,		// ref array@string				(24)
	OBJTYPE_RA_STRUCT,		// ref array@struct XXX			(25)

	OBJTYPE_IMainSystem,	// IMainSystem					(26)

	OBJTYPE_FUNCTYPE,		// functype						(27)
	OBJTYPE_V_FUNCTYPE,		// value functype				(28)
	OBJTYPE_VA_FUNCTYPE,	// value array@functype			(29)
	OBJTYPE_A_FUNCTYPE,		// array@functype				(30)
	OBJTYPE_R_FUNCTYPE,		// ref functype					(31)
	OBJTYPE_RA_FUNCTYPE,	// ref array@functype			(32)
	OBJTYPE_V_FUNC,			// value func					(33)

	OBJTYPE_THIS,			// this							(34)
	OBJTYPE_V_NULL,			// value NULL					(35)
	OBJTYPE_V_NEWSTRUCT,	// new struct XXX				(36)

	OBJTYPE_VR_INT,			// value ref int				(37)
	OBJTYPE_VR_FLOAT,		// value ref float				(38)
	OBJTYPE_VR_STRING,		// value ref string				(39)
	OBJTYPE_VR_STRUCT,		// value ref struct XXX			(40)
	OBJTYPE_VR_FUNCTYPE,	// value ref functype			(41)
	OBJTYPE_VRA_INT,		// value ref array@int			(42)
	OBJTYPE_VRA_FLOAT,		// value ref array@float		(43)
	OBJTYPE_VRA_STRING,		// value ref array@string		(44)
	OBJTYPE_VRA_STRUCT,		// value ref array@struct XXX	(45)
	OBJTYPE_VRA_FUNCTYPE,	// value ref array@functype		(46)

	OBJTYPE_BOOL,			// bool							(47)
	OBJTYPE_V_BOOL,			// value bool					(48)
	OBJTYPE_VA_BOOL,		// value array@bool				(49)
	OBJTYPE_A_BOOL,			// array@bool					(50)
	OBJTYPE_R_BOOL,			// ref bool						(51)
	OBJTYPE_RA_BOOL,		// ref array@bool				(52)
	OBJTYPE_VR_BOOL,		// value ref bool				(53)
	OBJTYPE_VRA_BOOL,		// value ref array@bool			(54)
};

#endif // __EOBJTYPE42_H__

