///////////////////////////////////////////////////////////////////////////////
//  GDecodeCG.h
//  Coder.Yudai Senoo :-)

#ifndef __GDECODECG_H__
#define __GDECODECG_H__

#include <objbase.h>
#include "IDecodeCG.h"

// {49DFC85D-8828-4a86-958C-4A6B37DF1FE9}
static const GUID IID_IDecodeCG = 
{ 0x49dfc85d, 0x8828, 0x4a86, { 0x95, 0x8c, 0x4a, 0x6b, 0x37, 0xdf, 0x1f, 0xe9 } };

#endif // __GDECODECG_H__

