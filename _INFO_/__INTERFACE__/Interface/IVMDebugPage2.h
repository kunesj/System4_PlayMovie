///////////////////////////////////////////////////////////////////////////////
//  IVMDebugPage2.h
//  Coder.Yudai Senoo :-)

#ifndef __IVMDEBUGPAGE2_H__
#define __IVMDEBUGPAGE2_H__

#include "IVMDebugPage.h"

interface IVMDebugPage2 : public IVMDebugPage {
	virtual int		GetArrayStruct(int nPage) = 0;		// 配列ページの構造体番号を取得する
	virtual int		GetRefCounter(int nPage) = 0;		// ページの参照カウンタを取得する
};

#endif // __IVMDEBUGPAGE2_H__

