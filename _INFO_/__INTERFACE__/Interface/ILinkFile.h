///////////////////////////////////////////////////////////////////////////////
//  ILinkFile.h
//  Coder.Yudai Senoo :-)

#ifndef __ILINKFILE_H__
#define __ILINKFILE_H__

#include "IInterface.h"

interface IMemory;

interface ILinkFile : public IInterface {
	virtual IMemory*	Get(int nNum) = 0;		// データの取得

};

#endif // __ILINKFILE_H__

