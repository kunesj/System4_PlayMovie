///////////////////////////////////////////////////////////////////////////////
//  GSurfaceFactory.h
//  Coder.Yudai Senoo :-)

#ifndef __GSURFACEFACTORY_H__
#define __GSURFACEFACTORY_H__

#include <objbase.h>

// {D6433348-4DB3-11d4-BEB2-00C0F6B0E9BE}
static const GUID IID_ISurfaceFactory = 
{ 0xd6433348, 0x4db3, 0x11d4, { 0xbe, 0xb2, 0x0, 0xc0, 0xf6, 0xb0, 0xe9, 0xbe } };

// AddRef()による参照カウンタが動作するバージョン
// {65C9F15F-260C-474e-8712-F6990745B61F}
static const GUID IID_ISurfaceFactory_RefCounter = 
{ 0x65c9f15f, 0x260c, 0x474e, { 0x87, 0x12, 0xf6, 0x99, 0x7, 0x45, 0xb6, 0x1f } };

// AddRef()による参照カウンタが動作するバージョン
// 8bppサーフェスが作成できるバージョン
// {F27189F4-9CA4-40a3-BE22-A332E2F192F8}
static const GUID IID_ISurfaceFactory_C = 
{ 0xf27189f4, 0x9ca4, 0x40a3, { 0xbe, 0x22, 0xa3, 0x32, 0xe2, 0xf1, 0x92, 0xf8 } };

#endif // __GSURFACEFACTORY_H__

