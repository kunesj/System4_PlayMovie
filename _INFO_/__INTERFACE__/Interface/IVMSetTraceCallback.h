///////////////////////////////////////////////////////////////////////////////
//  IVMSetTraceCallback.h
//  Coder.Yudai Senoo :-)

#ifndef __IVMSETTRACECALLBACK_H__
#define __IVMSETTRACECALLBACK_H__

#include <objbase.h>

interface IVMTraceCallback;

interface IVMSetTraceCallback {
	virtual void	SetTraceCallback(IVMTraceCallback* pIVMTraceCallback) = 0;
};

#endif // __IVMSETTRACECALLBACK_H__

