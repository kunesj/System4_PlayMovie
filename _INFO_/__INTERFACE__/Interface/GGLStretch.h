///////////////////////////////////////////////////////////////////////////////
//  GGLStretch.h
//  Coder.Yudai Senoo :-)

#ifndef __GGLSTRETCH_H__
#define __GGLSTRETCH_H__

#include <objbase.h>

// {6B0DD5E0-93CB-11d4-ACBF-00C0F6B0E9BE}
static const GUID IID_IGLStretch = 
{ 0x6b0dd5e0, 0x93cb, 0x11d4, { 0xac, 0xbf, 0x0, 0xc0, 0xf6, 0xb0, 0xe9, 0xbe } };

#endif // __GGLSTRETCH_H__

