///////////////////////////////////////////////////////////////////////////////
//  IReset.h
//  Coder.Yudai Senoo :-)

#ifndef __IRESET_H__
#define __IRESET_H__

#include <objbase.h>

interface IReset {
	virtual void	Reset(void) = 0;		// システムのリセット
};

#endif // __IRESET_H__

