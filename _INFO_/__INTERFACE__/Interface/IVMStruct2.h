///////////////////////////////////////////////////////////////////////////////
//  IVMStruct2.h
//  Coder.Yudai Senoo :-)

#ifndef __IVMSTRUCT2_H__
#define __IVMSTRUCT2_H__

#include "IVMStruct.h"

interface IVMStruct2 : public IVMStruct {
	// メンバオブジェクト取得
	virtual void*	GetDataByName(char* pszMember) = 0;
	//GetTypeで返ってくる値と、GetDataで取得できるポインタ種の対応表
	//	OBJTYPE_INT		int*
	//	OBJTYPE_FLOAT	float*
	//	OBJTYPE_STRING	IString*
	//	OBJTYPE_STRUCT	IVMStruct3*
	//	OBJTYPE_A*****	IVMArray*

	// メンバオブジェクト設定
	virtual bool	SetDataByName(char* pszMember, void* pvData) = 0;
	//GetTypeで返ってくる値と、SetDataで設定する内容の対応表
	//	OBJTYPE_INT		int*
	//	OBJTYPE_FLOAT	float*
	//	OBJTYPE_STRING	char*
	//	OBJTYPE_STRUCT	不可
	//	OBJTYPE_A*****	不可
};

#endif // __IVMSTRUCT2_H__

