///////////////////////////////////////////////////////////////////////////////
//  IMemory.h
//  Coder.Yudai Senoo :-)

#ifndef __IMEMORY_H__
#define __IMEMORY_H__

#include "IInterface.h"

interface IMemory : public IInterface {
	virtual bool	Alloc(int nSize) = 0;		// メモリの確保
	virtual bool	Realloc(int nSize) = 0;		// メモリの再確保
	virtual void	Free(void) = 0;				// メモリの解放

	virtual int		GetSize(void) = 0;			// サイズの取得
	virtual void*	GetPointer(void) = 0;		// ポインタの取得

};

#endif // __IMEMORY_H__

