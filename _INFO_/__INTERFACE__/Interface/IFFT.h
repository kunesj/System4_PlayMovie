#ifndef __IFFT_H__
#define __IFFT_H__

#include "IInterface.h"
#include <objbase.h>

interface IFFT : public IInterface
{
	virtual void	rdft(int n, int isgn, float* a)=0;
	virtual void	hanning_window(int n, float* a)=0;
};

#endif // __IFFT_H__
