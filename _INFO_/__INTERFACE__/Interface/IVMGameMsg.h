///////////////////////////////////////////////////////////////////////////////
//  IVMGameMsg.h
//  Coder.Yudai Senoo :-)

#ifndef __IVMGAMEMSG_H__
#define __IVMGAMEMSG_H__

#include <objbase.h>

interface IVMGameMsg {
	virtual int		GetNumof(void) = 0;			// メッセージ総数の取得
	
	virtual char*	Get(int nIndex) = 0;		// メッセージの取得
};

#endif // __IVMGAMEMSG_H__

