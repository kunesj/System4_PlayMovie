///////////////////////////////////////////////////////////////////////////////
//  ISys40Ini.h
//  Coder.Yudai Senoo :-)

#ifndef __ISYS40INI_H__
#define __ISYS40INI_H__

#include <objbase.h>

interface ISys40Ini {
	virtual int		GetVersion(void) = 0;
	
	virtual int		GetViewWidth(void) = 0;
	virtual int		GetViewHeight(void) = 0;
	
	virtual char*	GetGameName(void) = 0;
	virtual char*	GetRegName(void) = 0;
	virtual char*	GetCodeName(void) = 0;
	virtual char*	GetMainVMName(void) = 0;
	
	virtual char*	GetDLLFolderName(void) = 0;
	virtual char*	GetDLLName(int nNum) = 0;
};

#endif // __ISYS40INI_H__

