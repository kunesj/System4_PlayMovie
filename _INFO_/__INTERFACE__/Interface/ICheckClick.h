///////////////////////////////////////////////////////////////////////////////
//  ICheckClick.h
//  Coder.Yudai Senoo :-)

#ifndef __ICHECKCLICK_H__
#define __ICHECKCLICK_H__

#include <objbase.h>

interface ICheckClick {
	virtual void	Clear(void) = 0;
	virtual bool	IsClick(void) = 0;
};

#endif // __ICHECKCLICK_H__

