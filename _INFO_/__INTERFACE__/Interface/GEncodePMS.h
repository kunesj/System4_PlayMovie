///////////////////////////////////////////////////////////////////////////////
//  GEncodePMS.h
//  Coder.Shige :-)

#ifndef __GENCODEPMS_H__
#define __GENCODEPMS_H__

#include <objbase.h>

// {0056DEBC-7A14-8923-3250-416FAB8D7E9C}
static const GUID IID_IEncodePMS = 
{ 0x056debc, 0x7a14, 0x8923, { 0x32, 0x50, 0x41, 0x6f, 0xab, 0x8d, 0x7e, 0x9c } };

#endif // __GENCODEPMS_H__

