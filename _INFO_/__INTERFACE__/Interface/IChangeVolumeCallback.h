///////////////////////////////////////////////////////////////////////////////
//  IChangeVolumeCallback.h
//  Coder.Yudai Senoo :-)

#ifndef __ICHANGEVOLUMECALLBACK_H__
#define __ICHANGEVOLUMECALLBACK_H__

#include <objbase.h>

interface IChangeVolumeCallback {
	virtual void	OnChangeVolume(int nNum, int nVolume) = 0;
	virtual void	OnChangeMute(int nNum, bool bMute) = 0;
	virtual void	OnChangeDisable(int nNum, bool bDisable) = 0;
};

#endif // __ICHANGEVOLUMECALLBACK_H__

