///////////////////////////////////////////////////////////////////////////////
//  ISound2.h
//  Coder.Yudai Senoo :-)

#ifndef __ISOUND2_H__
#define __ISOUND2_H__

#include "IInterface.h"

interface IMainSystem;
interface IMusic;

interface ISound2 : public IInterface {
	virtual bool		Init(IMainSystem* pIMainSystem) = 0;		// 初期化

	virtual IMusic*		GetChannel(int nChannel) = 0;	// チャンネルの取得
	virtual int			GetUnuseChannel(void) = 0;		// 空きＷＡＶＥデバイスの取得(error : -1)
};

#endif // __ISOUND2_H__

