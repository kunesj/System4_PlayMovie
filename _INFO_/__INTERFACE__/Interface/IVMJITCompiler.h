///////////////////////////////////////////////////////////////////////////////
//  IVMJITCompiler.h
//  Coder.Yudai Senoo :-)

#ifndef __IVMJITCOMPILER_H__
#define __IVMJITCOMPILER_H__

#include <objbase.h>

interface IVMJITCompiler {
	// ＪＩＴコンパイラを実行するかどうかのフラグ設定
	virtual void	SetRunFlag(bool bFlag) = 0;
	
	virtual bool	GetRunFlag(void) = 0;
};

#endif // __IVMJITCOMPILER_H__

