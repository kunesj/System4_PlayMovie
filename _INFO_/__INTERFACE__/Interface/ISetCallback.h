///////////////////////////////////////////////////////////////////////////////
//  ISetCallback.h
//  Coder.Yudai Senoo :-)

#ifndef __ISETCALLBACK_H__
#define __ISETCALLBACK_H__

#include <objbase.h>

interface IInterface;

interface ISetCallback {
	virtual void	Set(IInterface* pIInterface) = 0;	// �o�^
};

#endif // __ISETCALLBACK_H__

