///////////////////////////////////////////////////////////////////////////////
//  ISleep.h
//  Coder.Yudai Senoo :-)

#ifndef __ISLEEP_H__
#define __ISLEEP_H__

#include <objbase.h>

interface ISleep {
	virtual void	Sleep(int nSleep) = 0;		// Win32API::Sleep()を呼び出す
};

#endif // __ISLEEP_H__

