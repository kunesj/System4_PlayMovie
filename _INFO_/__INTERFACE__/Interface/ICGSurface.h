///////////////////////////////////////////////////////////////////////////////
//  ICGSurface.h
//  Coder.Yudai Senoo :-)

#ifndef __ICGSURFACE_H__
#define __ICGSURFACE_H__

#include "ISurface.h"

interface ICGSurface : public ISurface {
	virtual void*	GetPalette(int nPal) = 0;		// パレット(RGBQUAD)へのポインタを取得
	virtual int		GetX(void) = 0;					// ＣＧ表示Ｘ位置
	virtual int		GetY(void) = 0;					// ＣＧ表示Ｙ位置
};

#endif // __ICGSURFACE_H__

