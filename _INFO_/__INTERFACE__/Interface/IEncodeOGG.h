///////////////////////////////////////////////////////////////////////////////
//  IEncodeOGG.h
//  Coder.Yudai Senoo :-)

#ifndef __IENCODEOGG_H__
#define __IENCODEOGG_H__

#include "IInterface.h"

interface IMemory;

interface IEncodeOGG : public IInterface {
	virtual int		GetVersion(void) = 0;
	virtual bool	Encode(void* pvData, int nSize, float fQuality, IMemory* pIResult) = 0;	// 圧縮
	/*
		fQuality : 0.0f 〜 1.0f
		エンコードクオリティの目安
		0.0		 64.0kbps
		0.1		 80.0kbps
		0.2		 96.0kbps
		0.3		112.0kbps
		0.4		128.0kbps
		0.5		160.0kbps
		0.6		192.0kbps
		0.7		224.0kbps
		0.8		256.0kbps
		0.9		320.0kbps
		1.0		500.0kbps
	*/
};

#endif // __IENCODEOGG_H__

