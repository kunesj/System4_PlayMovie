///////////////////////////////////////////////////////////////////////////////
//  IGLStretch.h
//  Coder.Yudai Senoo :-)

#ifndef __IGLSTRETCH_H__
#define __IGLSTRETCH_H__

#include "IInterface.h"

interface IMainSystem;
interface ISurface; 

interface IGLStretch : public IInterface {
	virtual bool	Init(IMainSystem* pIMainSystem) = 0;		// ������
	
	virtual void	BlendScreen2x2(ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;
	virtual void	BlendScreen2x2WDS(ISurface* pWriteSurface, int nWx, int nWy, ISurface* pDestSurface, int nDx, int nDy, ISurface* pSrcSurface, int nSx, int nSy, int nWidth, int nHeight) = 0;
	
};

#endif // __IGLSTRETCH_H__

