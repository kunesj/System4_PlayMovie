///////////////////////////////////////////////////////////////////////////////
//  IALDFileManager2.h
//  Coder.Yudai Senoo :-)

#ifndef __IALDFILEMANAGER2_H__
#define __IALDFILEMANAGER2_H__

#include "IALDFileManager.h"

interface IALDFileManager2 : public IALDFileManager {
	// レジストリを読んで、ファイル名を解決する
	virtual bool	SetFileNameFromRegistry(char* pszRegName) = 0;
};

#endif // __IALDFILEMANAGER2_H__

