///////////////////////////////////////////////////////////////////////////////
//  IWriteFile.h
//  Coder.Yudai Senoo :-)

#ifndef __IWRITEFILE_H__
#define __IWRITEFILE_H__

#include "IInterface.h"

interface IWriteFile : public IInterface {
	// データ追記書き込み
	virtual bool	Write(void* pvData, int nSize) = 0;
	virtual bool	WriteInt(int nData) = 0;
	virtual bool	WriteBYTE(unsigned char ucData) = 0;
	virtual bool	WriteWORD(unsigned short usData) = 0;
	virtual bool	WriteDWORD(unsigned int unData) = 0;
	virtual bool	WriteString(char* pszData) = 0;

	// データ位置指定書き込み
	virtual bool	WritePos(int nPos, void* pvData, int nSize) = 0;
	virtual bool	WritePosInt(int nPos, int nData) = 0;
	virtual bool	WritePosBYTE(int nPos, unsigned char ucData) = 0;
	virtual bool	WritePosWORD(int nPos, unsigned short usData) = 0;
	virtual bool	WritePosDWORD(int nPos, unsigned int unData) = 0;
	virtual bool	WritePosString(int nPos, char* pszData) = 0;

	// 情報
	virtual int		GetWritePos(void) = 0;
	virtual bool	SetWritePos(int nPos) = 0;
	virtual bool	SkipWritePos(int nSkip) = 0;

	// 保存
	virtual bool	Save(char* pszFileName) = 0;

};

#endif // __IWRITEFILE_H__

