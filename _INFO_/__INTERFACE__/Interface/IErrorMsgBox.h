///////////////////////////////////////////////////////////////////////////////
//  IErrorMsgBox.h
//  Coder.Yudai Senoo :-)

#ifndef __IERRORMSGBOX_H__
#define __IERRORMSGBOX_H__

#include <objbase.h>

interface IErrorMsgBox {
	virtual void	Error(char* pszText) = 0;	// エラーメッセージボックスを表示する
};

#endif // __IERRORMSGBOX_H__

