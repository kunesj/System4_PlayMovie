///////////////////////////////////////////////////////////////////////////////
//  GGraph.h
//  Coder.Yudai Senoo :-)

#ifndef __GGRAPH_H__
#define __GGRAPH_H__

#include <objbase.h>

// {58AD7D01-4DAE-11d4-BEB2-00C0F6B0E9BE}
static const GUID IID_IGraph = 
{ 0x58ad7d01, 0x4dae, 0x11d4, { 0xbe, 0xb2, 0x0, 0xc0, 0xf6, 0xb0, 0xe9, 0xbe } };

// {46F1EC26-82CD-4a69-A0BF-8F7D6975ABDA}
static const GUID IID_IGraph2 = 
{ 0x46f1ec26, 0x82cd, 0x4a69, { 0xa0, 0xbf, 0x8f, 0x7d, 0x69, 0x75, 0xab, 0xda } };

#endif // __GGRAPH_H__

