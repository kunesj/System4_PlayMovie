///////////////////////////////////////////////////////////////////////////////
//  IMusicLoader.h
//  Coder.Yudai Senoo :-)

#ifndef __IMUSICLOADER_H__
#define __IMUSICLOADER_H__

#include <objbase.h>

interface IMusicLoader {
	virtual bool	Load(void* pvData, int nSize) = 0;		// メモリから直接ロード
};

#endif // __IMUSICLOADER_H__

