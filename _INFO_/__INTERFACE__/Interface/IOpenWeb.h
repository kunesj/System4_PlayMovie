///////////////////////////////////////////////////////////////////////////////
//  IOpenWeb.h
//  Coder.Yudai Senoo :-)

#ifndef __IOPENWEB_H__
#define __IOPENWEB_H__

#include <objbase.h>

interface IOpenWeb {
	virtual void	Open(char* pszURL) = 0;		// ���������J��
};

#endif // __IOPENWEB_H__

