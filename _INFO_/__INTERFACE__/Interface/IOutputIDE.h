///////////////////////////////////////////////////////////////////////////////
//  IOutputIDE.h
//  Coder.Yudai Senoo :-)

#ifndef __IOUTPUTIDE_H__
#define __IOUTPUTIDE_H__

#include <objbase.h>

interface IOutputIDE {
	virtual void	Output(char* pszText) = 0;
};

#endif // __IOUTPUTIDE_H__

