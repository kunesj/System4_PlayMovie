///////////////////////////////////////////////////////////////////////////////
//  GEncodeQNT.h
//  Coder.Yudai Senoo :-)

#ifndef __GENCODEQNT_H__
#define __GENCODEQNT_H__

#include <objbase.h>

// {0BB1D361-659C-4025-A696-8E9AEBEADE3A}
static const GUID IID_IEncodeQNT = 
{ 0xbb1d361, 0x659c, 0x4025, { 0xa6, 0x96, 0x8e, 0x9a, 0xeb, 0xea, 0xde, 0x3a } };

#endif // __GENCODEQNT_H__

