///////////////////////////////////////////////////////////////////////////////
//  IS4xMusic.h
//  Coder. TYA-PA-

#ifndef __IS4XMUSIC_H__
#define __IS4XMUSIC_H__

#include "IInterface.h"

interface IMusic;
interface IMainSystem;

struct IS4xMusic : public IInterface {
	virtual void	Init(IMainSystem *pIMainSystem) = 0;
	//チャンネル指定でIMusicを取得する
	virtual IMusic	*GetIMusic(int nCh) = 0;
	//指定したリンク番号のデーターが存在するか？
	virtual bool	ExistData(int nIndex) = 0;

};

#endif // __IS4XMUSIC_H__

