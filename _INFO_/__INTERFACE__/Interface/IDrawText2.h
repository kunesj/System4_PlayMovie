///////////////////////////////////////////////////////////////////////////////
//  IDrawText2.h
//  Coder.Yudai Senoo :-)

#ifndef __IDRAWTEXT2_H__
#define __IDRAWTEXT2_H__

#include "IInterface.h"

interface ISurface;

interface IDrawText2 : public IInterface {
	virtual void	Draw(ISurface* pSurface, int nX, int nY, char* pszText) = 0;		// アンチ付きテキスト描画
	virtual void	DrawToAMap(ISurface* pSurface, int nX, int nY, char* pszText) = 0;	// アンチ付きテキスト描画

	// 設定
	virtual void	SetFontSize(int nSize) = 0;			// フォントのサイズ(最大128)
	virtual void	SetFontName(char* pszName) = 0;		// フォント名
	virtual void	SetWeight(int nWeight) = 0;			// フォントの太さ(0〜1000)
	virtual void	SetUnderline(bool bFlag) = 0;		// 下線を引くかどうかの指定
	virtual void	SetStrikeOut(bool bFlag) = 0;		// 打ち消し線を引くかどうかの指定
	virtual void	SetSpace(int nSpace) = 0;			// 字間隔

	virtual void	SetColor(int nR, int nG, int nB) = 0;	// 描画色

	// 取得
	virtual int		GetFontSize(void) = 0;				// フォントのサイズ
	virtual char*	GetFontName(void) = 0;				// フォント名
	virtual int		GetWeight(void) = 0;				// フォントの太さ(0〜1000)
	virtual bool	GetUnderline(void) = 0;				// 下線を引くかどうかの指定
	virtual bool	GetStrikeOut(void) = 0;				// 打ち消し線を引くかどうかの指定
	virtual int		GetSpace(void) = 0;					// 字間隔

	virtual void	GetColor(int* pnR, int* pnG, int* pnB) = 0;	// 描画色

};

/*
 *  Draw()が安全にクリッピングされる事は保証されます
 */

#endif // __IDRAWTEXT2_H__

