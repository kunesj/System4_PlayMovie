#ifndef __IMUSICMIXER2_H__
#define __IMUSICMIXER2_H__
//IMusicMixer.h
//kazushi

#include "IMusicMixer.h"
#include <objbase.h>

interface IMusicMixer2 : public IMusicMixer
{
	virtual bool	SetMasterVolume(float fPercent)=0;		//基本音量設定 0〜100(%)
	virtual bool	GetMasterVolume(float* pfPercent)=0;	//基本音量取得 0〜100(%)
	virtual bool	SetMuteStatus(bool bMute)=0;			//ミュート設定
	virtual bool	GetMuteStatus(bool* pbMute)=0;			//ミュート状態取得
};

#endif // __IMUSICMIXER2_H__
