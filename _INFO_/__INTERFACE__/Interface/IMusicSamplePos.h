///////////////////////////////////////////////////////////////////////////////
//  IMusicSamplePos.h
//  Coder.TYA-PA-

#ifndef __IMUSICSAMPLEPOS_H__
#define __IMUSICSAMPLEPOS_H__

#include <objbase.h>

typedef unsigned long DWORD;

interface IMusicSamplePos {
	virtual DWORD	GetPos(void) = 0;		// 再生位置（サンプル単位）の取得
	virtual DWORD	GetLength(void) = 0;	// 曲全体のサンプル数を取得
};

#endif // __IMUSICSAMPLEPOS_H__

