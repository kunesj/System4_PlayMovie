///////////////////////////////////////////////////////////////////////////////
//  GDrawText2.h
//  Coder.Yudai Senoo :-)

#ifndef __GDRAWTEXT2_H__
#define __GDRAWTEXT2_H__

#include <objbase.h>

// {387254BB-090F-4045-AA78-2521B1EA7072}
static const GUID IID_IDrawText2 = 
{ 0x387254bb, 0x90f, 0x4045, { 0xaa, 0x78, 0x25, 0x21, 0xb1, 0xea, 0x70, 0x72 } };

#endif // __GDRAWTEXT2_H__

