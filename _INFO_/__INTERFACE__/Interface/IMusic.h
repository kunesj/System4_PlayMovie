///////////////////////////////////////////////////////////////////////////////
//  IMusic.h
//  Coder.Yudai Senoo :-)

#ifndef __IMUSIC_H__
#define __IMUSIC_H__

#include "IInterface.h"

interface IMusic : public IInterface {
	virtual bool	Prepare(int nNum) = 0;		// 再生データの準備
	virtual bool	Unprepare(void) = 0;		// 再生データの破棄
	
	// 基本機能
	virtual bool	Play(void) = 0;				// 再生
	virtual bool	Stop(void) = 0;				// 停止
	virtual bool	IsPlay(void) = 0;			// 再生中かどうか
	
	// 拡張機能の取得
	virtual void*	GetInterface(const GUID* pGuid) = 0;

};

#endif // __IMUSIC_H__

