///////////////////////////////////////////////////////////////////////////////
//  IVMStruct.h
//  Coder.Yudai Senoo :-)

#ifndef __IVMSTRUCT_H__
#define __IVMSTRUCT_H__

#include <objbase.h>

interface IVMStruct {
	// 取得
	virtual char*	GetName(void) = 0;		// 構造体名
	virtual int		GetNumof(void) = 0;		// 個数

	// メンバオブジェクト取得
	virtual int		GetType(int nMember) = 0;
	virtual void*	GetData(int nMember) = 0;

	//GetTypeで返ってくる値と、GetDataで取得できるポインタ種の対応表
	//	OBJTYPE_INT		int*
	//	OBJTYPE_FLOAT	float*
	//	OBJTYPE_STRING	IVMString
	//	OBJTYPE_STRUCT	IVMStruct
	//	OBJTYPE_A*****	IVMArray

	// メンバオブジェクト設定1
	virtual bool	SetData(int nMember, void* pvData) = 0;
	//GetTypeで返ってくる値と、SetDataで設定する内容の対応表
	//	OBJTYPE_INT		int*
	//	OBJTYPE_FLOAT	float*
	//	OBJTYPE_STRING	char*
	//	OBJTYPE_STRUCT	不可
	//	OBJTYPE_A*****	不可
};

#endif // __IVMSTRUCT_H__

