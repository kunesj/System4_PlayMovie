///////////////////////////////////////////////////////////////////////////////
//  IDSPointer.h
//  Coder.Yudai Senoo :-)

#ifndef __IDSPOINTER_H__
#define __IDSPOINTER_H__

#include <objbase.h>

interface IDSPointer {
	virtual void*	Get(void) = 0;		// LPDIRECTSOUND�̎擾
};

#endif // __IDSPOINTER_H__

