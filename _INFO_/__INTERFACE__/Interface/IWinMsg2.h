///////////////////////////////////////////////////////////////////////////////
//  IWinMsg2.h
//  Coder.Yudai Senoo :-)

#ifndef __IWINMSG2_H__
#define __IWINMSG2_H__

#include "IWinMsg.h"

interface IWinMsg2 : public IWinMsg {
	virtual bool	GetPeekStatus(void) = 0;
};

#endif // __IWINMSG2_H__

