///////////////////////////////////////////////////////////////////////////////
//  GAlphaEffectCopy.h
//  Coder.Yudai Senoo :-)

#ifndef __GALPHAEFFECTCOPY_H__
#define __GALPHAEFFECTCOPY_H__

#include <objbase.h>

// {E1B12A27-F101-4c10-93A4-88868EB82890}
static const GUID IID_IAlphaEffectCopy = 
{ 0xe1b12a27, 0xf101, 0x4c10, { 0x93, 0xa4, 0x88, 0x86, 0x8e, 0xb8, 0x28, 0x90 } };

#endif // __GALPHAEFFECTCOPY_H__

