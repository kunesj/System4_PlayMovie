///////////////////////////////////////////////////////////////////////////////
//  IMsgBox.h
//  Coder.Yudai Senoo :-)

#ifndef __IMSGBOX_H__
#define __IMSGBOX_H__

#include <objbase.h>

interface IMsgBox {
	virtual void	MsgBox(char* pszText) = 0;
	virtual int		MsgBoxOkCancel(char* pszText) = 0;
};

#endif // __IMSGBOX_H__

