///////////////////////////////////////////////////////////////////////////////
//  IVMDebugMode.h
//  Coder.Yudai Senoo :-)

#ifndef __IVMDEBUGMODE_H__
#define __IVMDEBUGMODE_H__

#include <objbase.h>

interface IVMDebugMode {
	virtual bool	IsDebugMode(void) = 0;		// デバッグモードかどうか
};

#endif // __IVMDEBUGMODE_H__

