#ifndef __GMUSICSYSTEM_H__
#define __GMUSICSYSTEM_H__
//GMusicSystem.h
//kazushi

// {2271DBDB-DE6C-4878-97F0-A5B505B75B5F}
static const GUID IID_IMusicSystem = 
{ 0x2271dbdb, 0xde6c, 0x4878, { 0x97, 0xf0, 0xa5, 0xb5, 0x5, 0xb7, 0x5b, 0x5f } };

// {9B23F2E4-591E-44be-9F59-0E5302EC794E}
static const GUID IID_IFFT = 
{ 0x9b23f2e4, 0x591e, 0x44be, { 0x9f, 0x59, 0xe, 0x53, 0x2, 0xec, 0x79, 0x4e } };

#endif // __GMUSICSYSTEM_H__
