///////////////////////////////////////////////////////////////////////////////
//  IVMTraceCallback.h
//  Coder.Yudai Senoo :-)

#ifndef __IVMTRACECALLBACK_H__
#define __IVMTRACECALLBACK_H__

#include <objbase.h>

interface IVMTraceCallback {
	virtual bool	Call(unsigned int unAddr) = 0;
};

#endif // __IVMTRACECALLBACK_H__

