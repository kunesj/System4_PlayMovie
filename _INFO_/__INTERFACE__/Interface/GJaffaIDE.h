///////////////////////////////////////////////////////////////////////////////
//  GJaffaIDE.h
//  Coder.Yudai Senoo :-)

#ifndef __GJAFFAIDE_H__
#define __GJAFFAIDE_H__

#include <objbase.h>

// {CF24C4F1-CD2A-4791-97EE-6B162C148A71}
static const GUID IID_IOutputWindow = 
{ 0xcf24c4f1, 0xcd2a, 0x4791, { 0x97, 0xee, 0x6b, 0x16, 0x2c, 0x14, 0x8a, 0x71 } };

#endif // __GJAFFAIDE_H__

