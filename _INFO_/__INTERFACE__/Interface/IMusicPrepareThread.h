///////////////////////////////////////////////////////////////////////////////
//  IMusicPrepareThread.h
//  Coder.Yudai Senoo :-)

#ifndef __IMUSICPREPARETHREAD_H__
#define __IMUSICPREPARETHREAD_H__

#include <objbase.h>

interface IMusicPrepareThread {
	virtual bool	Prepare(int nNum) = 0;		// 再生データの準備
	virtual bool	IsEnd(void) = 0;			// 準備が終了したかどうか
	virtual void	Stop(void) = 0;				// 準備を停止する
	virtual void	WaitEnd(void) = 0;			// 準備が終了するまで待機する
};

#endif // __IMUSICPREPARETHREAD_H__

