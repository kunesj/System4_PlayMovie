///////////////////////////////////////////////////////////////////////////////
//  IMouseWheel.h
//  Coder.Yudai Senoo :-)

#ifndef __IMOUSEWHEEL_H__
#define __IMOUSEWHEEL_H__

#include <objbase.h>

interface IMouseWheel {
	virtual void	ClearCount(void) = 0;		// カウントのクリア
	
	virtual int		GetCountForward(void) = 0;	// 向こう側への回転
	virtual int		GetCountBack(void) = 0;		// 手前側への回転
};

#endif // __IMOUSEWHEEL_H__

