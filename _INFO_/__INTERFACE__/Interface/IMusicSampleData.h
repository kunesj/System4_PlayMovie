#ifndef __IMUSICSAMPLEDATA_H__
#define __IMUSICSAMPLEDATA_H__
//IMusicSampleData.h
//kazushi
//IMusic::GetInterface(&IID_IMusicSampleData)�Ŏ擾

#include <objbase.h>
interface IWaveFormat;

interface IMusicSampleData
{
	virtual	bool	Get(void* pvDst, unsigned int unSrcOffset, unsigned int unSize, unsigned int* punDone)=0;
	virtual bool	GetWaveFormat(IWaveFormat* pIWaveFormat)=0;
};

#endif // __IMUSICSAMPLEDATA_H__
