///////////////////////////////////////////////////////////////////////////////
//  GSound2.h
//  Coder.Yudai Senoo :-)

#ifndef __GSOUND2_H__
#define __GSOUND2_H__

#include <objbase.h>

// {4F6F7E1C-B914-497e-B29C-13067DA66F43}
static const GUID IID_ISound2 = 
{ 0x4f6f7e1c, 0xb914, 0x497e, { 0xb2, 0x9c, 0x13, 0x6, 0x7d, 0xa6, 0x6f, 0x43 } };

#endif // __GSOUND2_H__

