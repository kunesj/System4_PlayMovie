///////////////////////////////////////////////////////////////////////////////
//  IVMDebugPage.h
//  Coder.Yudai Senoo :-)

#ifndef __IVMDEBUGPAGE_H__
#define __IVMDEBUGPAGE_H__

#include <objbase.h>

interface IMemory;

interface IVMDebugPage {
	virtual int			GetNumofPage(void) = 0;			// 最大ページ数
	virtual IMemory*	GetPage(int nPage) = 0;			// ページメモリの取得
	// (注)解放されているページはNULLを返す

	virtual int			GetCurrentLocal(void) = 0;		// 現在のローカルページ番号の取得
	virtual int			GetCurrentFunc(void) = 0;		// 現在呼び出されている関数番号の取得
	virtual int			GetCurrentStruct(void) = 0;		// 現在呼び出されているメンバ関数番号の取得
	// (注)通常関数が呼び出されてる場合は-1を返す

	virtual int			GetPageType(int nPage)=0;		// ページの種類の取得
	virtual int			GetFunc(int nPage) = 0;			// ページ番号から関数番号を取得
	// (注)関数ページでない場合は-1を返す
	virtual int			GetStruct(int nPage) = 0;		// ページ番号から構造体番号を得る
	// (注)構造体ページでない場合は-1を返す

	virtual int			GetGlobalPage(void) = 0;		// グローバルページ番号の取得
	virtual int			GetArrayDepth(int nPage) = 0;	// 配列の場合、深さを返す。底面０
	virtual int			GetArrayType(int nPage) = 0;	// ページが配列の場合、配列の種類を返す。

	virtual int			GetFuncStackNumof(void) = 0;	// 関数の呼び出し階層の数を返す。
	virtual int			GetFuncStack(int nIndex) = 0;	// 関数の呼び出し階層のページ番号を返す。
};

#endif // __IVMDEBUGPAGE_H__

