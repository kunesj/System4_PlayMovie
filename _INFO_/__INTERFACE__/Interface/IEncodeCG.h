///////////////////////////////////////////////////////////////////////////////
//  IEncodeCG.h
//  Coder.Yudai Senoo :-)

#ifndef __IENCODECG_H__
#define __IENCODECG_H__

#include "IInterface.h"

interface IMemory;
interface ISurface;
interface IColorTable;

interface IEncodeCG : public IInterface {
	virtual IMemory*	Encode(ISurface* pISurface, IColorTable* pIColorTable) = 0;	// ���k
};

#endif // __IENCODECG_H__

