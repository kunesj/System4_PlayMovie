///////////////////////////////////////////////////////////////////////////////
//  GEncodeROU.h
//  Coder.Yudai Senoo :-)

#ifndef __GENCODEROU_H__
#define __GENCODEROU_H__

#include <objbase.h>

// {E30D0D3E-8C4C-4047-A1F0-4606A13038B0}
static const GUID IID_IEncodeROU = 
{ 0xe30d0d3e, 0x8c4c, 0x4047, { 0xa1, 0xf0, 0x46, 0x6, 0xa1, 0x30, 0x38, 0xb0 } };

#endif // __GENCODEROU_H__

