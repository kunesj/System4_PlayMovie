///////////////////////////////////////////////////////////////////////////////
//  IURLMenu.h
//  Coder.Yudai Senoo :-)

#ifndef __IURLMENU_H__
#define __IURLMENU_H__

#include <objbase.h>

interface IURLMenu {
	virtual bool	Add(char* pszTitle, char* pszURL) = 0;
};

#endif // __IURLMENU_H__

