///////////////////////////////////////////////////////////////////////////////
//  IFullScreen.h
//  Coder.Yudai Senoo :-)

#ifndef __IFULLSCREEN_H__
#define __IFULLSCREEN_H__

#include <objbase.h>

interface IFullScreen {
	// 現在フルスクリーン状態かどうかを調べる
	virtual bool	IsFullScreen(void) = 0;
	
	// 状態変更
	virtual bool	ChangeFullScreen(void) = 0;
	virtual bool	ChangeNormalScreen(void) = 0;
};

#endif // __IFULLSCREEN_H__

