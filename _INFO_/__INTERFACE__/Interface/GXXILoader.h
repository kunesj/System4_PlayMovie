///////////////////////////////////////////////////////////////////////////////
//  GXXILoader.h
//  Coder.Yudai Senoo :-)

#ifndef __GXXILOADER_H__
#define __GXXILOADER_H__

#include <objbase.h>

// {614E22EE-F8C1-4389-B3DA-59CB21E62CEF}
static const GUID IID_IXXILoader = 
{ 0x614e22ee, 0xf8c1, 0x4389, { 0xb3, 0xda, 0x59, 0xcb, 0x21, 0xe6, 0x2c, 0xef } };

// {4C0B1008-FFB0-4f37-8163-101DC2582E8C}
static const GUID IID_IXXILoader2 = 
{ 0x4c0b1008, 0xffb0, 0x4f37, { 0x81, 0x63, 0x10, 0x1d, 0xc2, 0x58, 0x2e, 0x8c } };

#endif // __GXXILOADER_H__

