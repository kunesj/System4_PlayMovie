///////////////////////////////////////////////////////////////////////////////
//  IString.h
//  Coder.Yudai Senoo :-)

#ifndef __ISTRING_H__
#define __ISTRING_H__

#include <objbase.h>

interface IString {
	virtual char*	Get(void) = 0;				// 文字列の取得
	virtual void	Set(char* pszText) = 0;		// 文字列の設定
};

#endif // __ISTRING_H__

