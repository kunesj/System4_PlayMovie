///////////////////////////////////////////////////////////////////////////////
//  IGLEffectCopyQuake.h
//  Coder.Yudai Senoo :-)

#ifndef __IGLEFFECTCOPYQUAKE_H__
#define __IGLEFFECTCOPYQUAKE_H__

#include "IInterface.h"

interface IGLEffectCopyQuake : public IInterface {
	// �ݒ�
	virtual void	SetMode(int nMode) = 0;		// ���(�����l=0)
	virtual void	SetAmp(int nAmp) = 0;		// �U����(�����l=128)
	virtual void	SetAmpX(int nAmpX) = 0;		// X�U����(�����l=128)
	virtual void	SetAmpY(int nAmpY) = 0;		// Y�U����(�����l=128)
	virtual void	SetCycle(int nCycle) = 0;	// ��]��(�����l=3)
	
	// �擾
	virtual int		GetMode(void) = 0;		// ���
	virtual int		GetAmp(void) = 0;		// �U����
	virtual int		GetAmpX(void) = 0;		// X�U����
	virtual int		GetAmpY(void) = 0;		// Y�U����
	virtual int		GetCycle(void) = 0;		// ��]��
};

#endif // __IGLEFFECTCOPYQUAKE_H__

