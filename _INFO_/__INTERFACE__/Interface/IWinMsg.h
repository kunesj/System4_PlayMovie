///////////////////////////////////////////////////////////////////////////////
//  IWinMsg.h
//  Coder.Yudai Senoo :-)

#ifndef __IWINMSG_H__
#define __IWINMSG_H__

#include <objbase.h>

interface IWinMsg {
	virtual bool	Peek(void) = 0;		// メッセージピーク
};

#endif // __IWINMSG_H__

