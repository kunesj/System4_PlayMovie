///////////////////////////////////////////////////////////////////////////////
//  IGLEffectCopy.h
//  Coder.Yudai Senoo :-)

#ifndef __IGLEFFECTCOPY_H__
#define __IGLEFFECTCOPY_H__

#include "IInterface.h"

interface IWinMsg;
interface ICheckClick;
interface ITimer;
interface IMainSurface;
interface ISurface;

interface IGLEffectCopy : public IInterface {
	// インターフェイスの設定
	virtual void	SetIWinMsg(IWinMsg* pIWinMsg) = 0;
	virtual void	SetICheckClick(ICheckClick* pICheckClick) = 0;
	virtual void	SetITimer(ITimer* pITimer) = 0;

	virtual bool	Effect(int nEffect,
		IMainSurface* pWriteSurface, int nWx, int nWy,
		ISurface* pDestSurface, int nDx, int nDy,
		ISurface* pSrcSurface, int nSx, int nSy,
		int nWidth, int nHeight,
		unsigned int unTotalTime,
		unsigned int* punEndType) = 0;
	// 戻り値 == false                     : WinMsgによる終了
	// 戻り値 == true && *punEndType == -1 : nEffectが正常でない場合
	// 戻り値 == true && *punEndType ==  0 : 正常終了
	// 戻り値 == true && *punEndType ==  1 : キー抜けによる終了

};

#endif // __IGLEFFECTCOPY_H__

