///////////////////////////////////////////////////////////////////////////////
//  IJaffaDebugPlugin.h
//  Coder.Yudai Senoo :-)

#ifndef __IJAFFADEBUGPLUGIN_H__
#define __IJAFFADEBUGPLUGIN_H__

#include <objbase.h>

interface IJaffaDebug;

interface IJaffaDebugPlugin {
	virtual bool	Init(IJaffaDebug* pIJaffaDebug) = 0;
	
	virtual bool	Open(void) = 0;
	virtual bool	Close(void) = 0;
	
	virtual char*	GetName(void) = 0;
};

#endif // __IJAFFADEBUGPLUGIN_H__

