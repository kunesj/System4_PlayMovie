#ifndef _LIB_PLAYMOVIE_H_
#define _LIB_PLAYMOVIE_H_

#include <iostream>
#include <string.h>

#if defined _WIN32 || defined __CYGWIN__
    #ifdef __GNUC__
        #define DLL_PUBLIC __attribute__ ((dllexport))
    #else
        #define DLL_PUBLIC __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
    #endif
    #define DLL_LOCAL
#else
    #if __GNUC__ >= 4
        #define DLL_PUBLIC __attribute__ ((visibility ("default")))
        #define DLL_LOCAL  __attribute__ ((visibility ("hidden")))
    #else
        #define DLL_PUBLIC
        #define DLL_LOCAL
    #endif
#endif

/* Make sure functions are exported with C linkage under C++ compilers. */
extern "C"
{
    /**
    Returns:
        true - inited without problem
        false - problem when init 
            system.Error("ムービー再生の初期化に失敗しました");
    **/
    DLL_PUBLIC bool Init( void );

    /**
    Returns:
        true - movie playing
        false - movie not playing (finished)
    **/
    DLL_PUBLIC bool IsPlay( void );

    /**
    Returns:
        true - file loaded correctly
        false - error when loading file
            system.Error("ムービーファイル【 %s 】\nの読み込みに失敗しました" % szFileName);
    **/
    DLL_PUBLIC bool Load( char* szFileName );

    /**
    Returns:
        true - started playback without problem
        false - error when starting playback
            system.Error("ムービー再生に失敗しました");
    **/
    DLL_PUBLIC bool Play( void );

    /**
    Returns:
        true - no error
        false - error
    **/
    DLL_PUBLIC bool Release( void ); 

    /**
    Returns:
        true - no error
        false - error
    **/
    DLL_PUBLIC bool Stop( void );
}
 
#endif
