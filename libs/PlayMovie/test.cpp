#include <iostream>
#include <string.h>

#include "PlayMovie.hpp"
 
int main()
{    
    char* filename = (char*)"Rance7.alm";
    
    std::cout << Init() << std::endl;
    std::cout << IsPlay() << std::endl;
    std::cout << Load(filename)  << std::endl;
    std::cout << Play()  << std::endl;
    std::cout << Release()  << std::endl;
    std::cout << Stop()  << std::endl;
    
    return(0); // No Error
}
