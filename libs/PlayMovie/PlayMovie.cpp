#include "PlayMovie.hpp"


DLL_PUBLIC bool Init( void )
{
    std::cout << "PlayMovie:Init" << std::endl;
    return true;
}

DLL_PUBLIC bool IsPlay( void ) 
{
    std::cout << "PlayMovie:IsPlay" << std::endl;
    return false;
}

DLL_PUBLIC bool Load( char* szFileName )
{
    std::cout << "PlayMovie:Load" << std::endl;
    
    for(char* it = szFileName; *it; ++it) {
        std::cout << std::hex << int(*it) << " ";
    }
    std::cout << std::endl;
    
    /*for(char* it = szFileName; *it; ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;*/
    
    //std::cout << szFileName << std::endl;
    
    return true;
}

DLL_PUBLIC bool Play( void )
{
    std::cout << "PlayMovie:Play" << std::endl;
    return true;
}

DLL_PUBLIC bool Release( void ) 
{
    std::cout << "PlayMovie:Release" << std::endl;
    return true;
}

DLL_PUBLIC bool Stop( void )
{
    std::cout << "PlayMovie:Stop" << std::endl;
    return true;
}
