#ifndef _LIB_DRAWGRAPH_H_
#define _LIB_DRAWGRAPH_H_

#include <iostream>
//#include <string.h>

#if defined _WIN32 || defined __CYGWIN__
 #ifdef __GNUC__
  #define DLL_PUBLIC __attribute__ ((dllexport))
 #else
  #define DLL_PUBLIC __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
 #endif
 #define DLL_LOCAL
#else
 #if __GNUC__ >= 4
  #define DLL_PUBLIC __attribute__ ((visibility ("default")))
  #define DLL_LOCAL  __attribute__ ((visibility ("hidden")))
 #else
  #define DLL_PUBLIC
  #define DLL_LOCAL
 #endif
#endif

#include "IInterface.h"

/**
Documentation is Gooogle translated from Japanese System40 SDK documentation.
**/

extern "C"
{

// Features: Copy
DLL_PUBLIC void Copy(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);
DLL_PUBLIC void CopyBright(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nRate);
DLL_PUBLIC void CopyAMap(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);
DLL_PUBLIC void CopySprite(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nR, int nG, int nB);
DLL_PUBLIC void CopyColorReverse(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);

// Features: Comparison copy
DLL_PUBLIC void CopyUseAMapUnder(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nAlpha);
DLL_PUBLIC void CopyUseAMapBorder(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nAlpha);
DLL_PUBLIC void CopyAMapMax(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);
DLL_PUBLIC void CopyAMapMin(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);

// Function: α value reference blend
DLL_PUBLIC void Blend(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nAlpha);
DLL_PUBLIC void BlendSrcBright(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nAlpha, int nRate);
DLL_PUBLIC void BlendAddSatur(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);

// Function: α map reference blend
DLL_PUBLIC void BlendAMap(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);            // DestPixel = Blend(DestPixel, SrcPixel, SrcAMap)
DLL_PUBLIC void BlendAMapSrcOnly(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);           // DestPixel = Satur(DestPixel + Src x AMap)
DLL_PUBLIC void BlendAMapColor(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nR, int nG, int nB);       // DestPixel = Blend(DestPixel, Color, SrcAMap)
DLL_PUBLIC void BlendAMapColorAlpha(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nR, int nG, int nB, int nAlpha);  // DestPixel = Blend(DestPixel, Color, SrcAMap x nAlpha)
DLL_PUBLIC void BlendAMapAlpha(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nAlpha);          // DestPixel = Blend(DestPixel, SrcPixel, SrcAMap x nAlpha)
DLL_PUBLIC void BlendAMapBright(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nRate);          // DestPixel = Blend(DestPixel, SrcPixel x nRate, SrcAMap)
DLL_PUBLIC void BlendAMapAlphaSrcBright(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nAlpha, int nRate);     // DestPixel = Blend(DestPixel, SrcPixel x nRate, SrcAMap x nAlpha)
DLL_PUBLIC void BlendUseAMapColor(int nDest, int nDx, int nDy, int nAlpha, int nAx, int nAy, int nWidth, int nHeight, int nR, int nG, int nB, int nRate);

// Features: filter blend
DLL_PUBLIC void BlendScreen(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);      // Filter blend: Screen
DLL_PUBLIC void BlendMultiply(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);    // Filter Blend: multiplication
DLL_PUBLIC void BlendScreenAlpha(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nAlpha); // Filter Blend: screen Xα

// Features: Fill
DLL_PUBLIC void Fill(int nDest, int nX, int nY, int nWidth, int nHeight, int nR, int nG, int nB);
DLL_PUBLIC void FillAlphaColor(int nDest, int nX, int nY, int nWidth, int nHeight, int nR, int nG, int nB, int nRate);
DLL_PUBLIC void FillAMap(int nDest, int nX, int nY, int nWidth, int nHeight, int nAlpha);
DLL_PUBLIC void FillAMapOverBorder(int nDest, int nX, int nY, int nWidth, int nHeight, int nAlpha, int nBorder);
DLL_PUBLIC void FillAMapUnderBorder(int nDest, int nX, int nY, int nWidth, int nHeight, int nAlpha, int nBorder);
DLL_PUBLIC void FillAMapGradationUD(int nDest, int nX, int nY, int nWidth, int nHeight, int nUpA, int nDownA);
DLL_PUBLIC void FillScreen(int nDest, int nX, int nY, int nWidth, int nHeight, int nR, int nG, int nB);
DLL_PUBLIC void FillMultiply(int nDest, int nX, int nY, int nWidth, int nHeight, int nR, int nG, int nB);

DLL_PUBLIC void SaturDP_DPxSA(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);

// Features: α＝α X α
DLL_PUBLIC void ScreenDA_DAxSA(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);       // DestAlpha = Screen(DestAlpha, SrcAlpha)
DLL_PUBLIC void AddDA_DAxSA(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);       // DestAlpha = Satur(DestAlpha + SrcAlpha)
DLL_PUBLIC void SpriteCopyAMap(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nColorKey);
DLL_PUBLIC void BlendDA_DAxSA(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);     // DestAlpha = Screen(DestAlpha, SrcAlpha)
DLL_PUBLIC void SubDA_DAxSA(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);

// Features: rectangle effect
DLL_PUBLIC void BrightDestOnly(int nDest, int nX, int nY, int nWidth, int nHeight, int nRate);

// Features: texture wrap
DLL_PUBLIC void CopyTextureWrap(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight, int nU, int nV);
DLL_PUBLIC void CopyTextureWrapAlpha(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight, int nU, int nV, int nAlpha);

// Features: scaling
DLL_PUBLIC void CopyStretch(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight);
DLL_PUBLIC void CopyStretchBlend(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight, int nAlpha);
DLL_PUBLIC void CopyStretchBlendAMap(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight);
DLL_PUBLIC void CopyStretchAMap(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight);   // scaling of α map
DLL_PUBLIC void CopyStretchInterp(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight);    // Interpolation scaling (expanding direction)
DLL_PUBLIC void CopyStretchAMapInterp(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight);   // α map interpolation scaling (expansion direction)

DLL_PUBLIC void CopyReduce(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight);     // Interpolation scaling (reduction orientation)
DLL_PUBLIC void CopyReduceAMap(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight);    // α map interpolation scaling (reduction orientation)

// Features: character drawing
DLL_PUBLIC void DrawTextToPMap(int nDest, int nX, int nY, char* szText); // TODO original datatypes (int nDest, int nX, int nY, string szText)
DLL_PUBLIC void DrawTextToAMap(int nDest, int nX, int nY, char* szText); // TODO original datatypes (int nDest, int nX, int nY, string szText)

// Function: Fonts
DLL_PUBLIC void SetFontSize(int nSize);
DLL_PUBLIC void SetFontName(char* pIText); // TODO original datatypes (string pIText)
DLL_PUBLIC void SetFontWeight(int nWeight);
DLL_PUBLIC void SetFontUnderline(int nFlag);
DLL_PUBLIC void SetFontStrikeOut(int nFlag);
DLL_PUBLIC void SetFontSpace(int nSpace);
DLL_PUBLIC void SetFontColor(int nR, int nG, int nB);
DLL_PUBLIC int  GetFontSize(void);
DLL_PUBLIC char* GetFontName(void); // TODO returns "string"
DLL_PUBLIC int  GetFontWeight(void);
DLL_PUBLIC int  GetFontUnderline(void);
DLL_PUBLIC int  GetFontStrikeOut(void);
DLL_PUBLIC int  GetFontSpace(void);
DLL_PUBLIC void GetFontColor(int* pnR, int* pnG, int* pnB); // TODO original datatypes (intp pnR, intp pnG, intp pnB)

// Features: Rotation scaling
DLL_PUBLIC void CopyRotZoom(int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);
DLL_PUBLIC void CopyRotZoomAMap(int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);
DLL_PUBLIC void CopyRotZoomUseAMap(int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);

DLL_PUBLIC void CopyRotZoom2Bilinear(int nDest, float fCx, float fCy, int nSrc, float fSrcCx, float fSrcCy, float fRotate, float fMag);

// Features: Y-axis rotation
DLL_PUBLIC void CopyRotateY(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);
DLL_PUBLIC void CopyRotateYUseAMap(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);
DLL_PUBLIC void CopyRotateYFixL(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);
DLL_PUBLIC void CopyRotateYFixR(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);
DLL_PUBLIC void CopyRotateYFixLUseAMap(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);
DLL_PUBLIC void CopyRotateYFixRUseAMap(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);

// Features: X-axis rotation
DLL_PUBLIC void CopyRotateX(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);
DLL_PUBLIC void CopyRotateXUseAMap(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);
DLL_PUBLIC void CopyRotateXFixU(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);
DLL_PUBLIC void CopyRotateXFixD(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);
DLL_PUBLIC void CopyRotateXFixUUseAMap(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);
DLL_PUBLIC void CopyRotateXFixDUseAMap(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag);

// Features: inverted
DLL_PUBLIC void CopyReverseLR(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);
DLL_PUBLIC void CopyReverseUD(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);
DLL_PUBLIC void CopyReverseAMapLR(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);
DLL_PUBLIC void CopyReverseAMapUD(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight);

// Features: blur
DLL_PUBLIC void CopyWidthBlur(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nBlur);
DLL_PUBLIC void CopyHeightBlur(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nBlur);
DLL_PUBLIC void CopyAMapWidthBlur(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nBlur);
DLL_PUBLIC void CopyAMapHeightBlur(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nBlur);

// Features: line drawing
DLL_PUBLIC void DrawLine(int nDest, int nX0, int nY0, int nX1, int nY1, int nR, int nG, int nB);
DLL_PUBLIC void DrawLineToAMap(int nDest, int nX0, int nY0, int nX1, int nY1, int nAlpha);

// Features: value obtained
DLL_PUBLIC bool GetPixelColor(int nSurface, int nX, int nY, int nR, int nG, int nB); // TODO original datatypes (int nSurface, int nX, int nY, ref int nR, ref int nG, ref int nB);
DLL_PUBLIC bool GetAlphaColor(int nSurface, int nX, int nY, int nA);  // TODO original datatypes (int nSurface, int nX, int nY, ref int nA)

// Features: polygon drawing
DLL_PUBLIC void DrawPolygon(int nDest, int nTex,
       float fX0, float fY0, float fZ0, float fU0, float fV0,
       float fX1, float fY1, float fZ1, float fU1, float fV1,
       float fX2, float fY2, float fZ2, float fU2, float fV2);

DLL_PUBLIC void DrawColorPolygon(int nDest,
     float fX0, float fY0, float fZ0, int nR0, int nG0, int nB0, int nA0,
     float fX1, float fY1, float fZ1, int nR1, int nG1, int nB1, int nA1,
     float fX2, float fY2, float fZ2, int nR2, int nG2, int nB2, int nA2);
     

/**
Functions not defined in SDK documentation
Decompilled parameters == unknown paramter types
**/
// TODO all parameters may be wrong
DLL_PUBLIC IInterface* CreateInterface(const void *a1);
DLL_PUBLIC char* FillOverlay(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8);
DLL_PUBLIC char* FillOverlayAlpha(int a1, int a2, int a3, int a4, int a5, signed int a6, signed int a7, int a8, int a9);

}

#endif

