#include "DrawGraph.hpp"


// Features: Copy
DLL_PUBLIC void Copy(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyBright(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nRate){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyAMap(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopySprite(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nR, int nG, int nB){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyColorReverse(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}

// Features: Comparison copy
DLL_PUBLIC void CopyUseAMapUnder(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nAlpha){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyUseAMapBorder(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nAlpha){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyAMapMax(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyAMapMin(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}

// Function: α value reference blend
DLL_PUBLIC void Blend(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nAlpha){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void BlendSrcBright(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nAlpha, int nRate){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void BlendAddSatur(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}

// Function: α map reference blend
DLL_PUBLIC void BlendAMap(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}            
DLL_PUBLIC void BlendAMapSrcOnly(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}         
DLL_PUBLIC void BlendAMapColor(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nR, int nG, int nB){std::cout << "DrawGraph:???" << std::endl;}      
DLL_PUBLIC void BlendAMapColorAlpha(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nR, int nG, int nB, int nAlpha){std::cout << "DrawGraph:???" << std::endl;}  
DLL_PUBLIC void BlendAMapAlpha(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nAlpha){std::cout << "DrawGraph:???" << std::endl;}         
DLL_PUBLIC void BlendAMapBright(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nRate){std::cout << "DrawGraph:???" << std::endl;}        
DLL_PUBLIC void BlendAMapAlphaSrcBright(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nAlpha, int nRate){std::cout << "DrawGraph:???" << std::endl;}     
DLL_PUBLIC void BlendUseAMapColor(int nDest, int nDx, int nDy, int nAlpha, int nAx, int nAy, int nWidth, int nHeight, int nR, int nG, int nB, int nRate){std::cout << "DrawGraph:???" << std::endl;}

// Features: filter blend
DLL_PUBLIC void BlendScreen(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}      
DLL_PUBLIC void BlendMultiply(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}   
DLL_PUBLIC void BlendScreenAlpha(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nAlpha){std::cout << "DrawGraph:???" << std::endl;} 

// Features: Fill
DLL_PUBLIC void Fill(int nDest, int nX, int nY, int nWidth, int nHeight, int nR, int nG, int nB){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void FillAlphaColor(int nDest, int nX, int nY, int nWidth, int nHeight, int nR, int nG, int nB, int nRate){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void FillAMap(int nDest, int nX, int nY, int nWidth, int nHeight, int nAlpha){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void FillAMapOverBorder(int nDest, int nX, int nY, int nWidth, int nHeight, int nAlpha, int nBorder){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void FillAMapUnderBorder(int nDest, int nX, int nY, int nWidth, int nHeight, int nAlpha, int nBorder){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void FillAMapGradationUD(int nDest, int nX, int nY, int nWidth, int nHeight, int nUpA, int nDownA){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void FillScreen(int nDest, int nX, int nY, int nWidth, int nHeight, int nR, int nG, int nB){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void FillMultiply(int nDest, int nX, int nY, int nWidth, int nHeight, int nR, int nG, int nB){std::cout << "DrawGraph:???" << std::endl;}

DLL_PUBLIC void SaturDP_DPxSA(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}

// Features: α＝α X α
DLL_PUBLIC void ScreenDA_DAxSA(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}     
DLL_PUBLIC void AddDA_DAxSA(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}      
DLL_PUBLIC void SpriteCopyAMap(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nColorKey){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void BlendDA_DAxSA(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}     
DLL_PUBLIC void SubDA_DAxSA(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}

// Features: rectangle effect
DLL_PUBLIC void BrightDestOnly(int nDest, int nX, int nY, int nWidth, int nHeight, int nRate){std::cout << "DrawGraph:???" << std::endl;}

// Features: texture wrap
DLL_PUBLIC void CopyTextureWrap(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight, int nU, int nV){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyTextureWrapAlpha(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight, int nU, int nV, int nAlpha){std::cout << "DrawGraph:???" << std::endl;}

// Features: scaling
DLL_PUBLIC void CopyStretch(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyStretchBlend(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight, int nAlpha){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyStretchBlendAMap(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyStretchAMap(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight){std::cout << "DrawGraph:???" << std::endl;}   
DLL_PUBLIC void CopyStretchInterp(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight){std::cout << "DrawGraph:???" << std::endl;}   
DLL_PUBLIC void CopyStretchAMapInterp(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight){std::cout << "DrawGraph:???" << std::endl;}   

DLL_PUBLIC void CopyReduce(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight){std::cout << "DrawGraph:???" << std::endl;}     
DLL_PUBLIC void CopyReduceAMap(int nDest, int nDx, int nDy, int nDWidth, int nDHeight, int nSrc, int nSx, int nSy, int nSWidth, int nSHeight){std::cout << "DrawGraph:???" << std::endl;}    

// Features: character drawing
DLL_PUBLIC void DrawTextToPMap(int nDest, int nX, int nY, char* szText){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void DrawTextToAMap(int nDest, int nX, int nY, char* szText){std::cout << "DrawGraph:???" << std::endl;}

// Function: Fonts
DLL_PUBLIC void SetFontSize(int nSize){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void SetFontName(char* pIText){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void SetFontWeight(int nWeight){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void SetFontUnderline(int nFlag){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void SetFontStrikeOut(int nFlag){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void SetFontSpace(int nSpace){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void SetFontColor(int nR, int nG, int nB){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC int  GetFontSize(void)
{
    std::cout << "DrawGraph:???" << std::endl;
    return 12; // TODO
}
DLL_PUBLIC char* GetFontName(void)
{
    std::cout << "DrawGraph:???" << std::endl;
    char* ret = (char*)"lol";
    return ret; // TODO
}
DLL_PUBLIC int  GetFontWeight(void)
{
    std::cout << "DrawGraph:???" << std::endl;
    return 0; // TODO
}
DLL_PUBLIC int  GetFontUnderline(void)
{
    std::cout << "DrawGraph:???" << std::endl;
    return 0; // TODO
}
DLL_PUBLIC int  GetFontStrikeOut(void)
{
    std::cout << "DrawGraph:???" << std::endl;
    return 0; // TODO
}
DLL_PUBLIC int  GetFontSpace(void)
{
    std::cout << "DrawGraph:???" << std::endl;
    return 0; // TODO
}
DLL_PUBLIC void GetFontColor(int* pnR, int* pnG, int* pnB){std::cout << "DrawGraph:???" << std::endl;} 

// Features: Rotation scaling
DLL_PUBLIC void CopyRotZoom(int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyRotZoomAMap(int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyRotZoomUseAMap(int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}

DLL_PUBLIC void CopyRotZoom2Bilinear(int nDest, float fCx, float fCy, int nSrc, float fSrcCx, float fSrcCy, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}

// Features: Y-axis rotation
DLL_PUBLIC void CopyRotateY(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyRotateYUseAMap(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyRotateYFixL(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyRotateYFixR(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyRotateYFixLUseAMap(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyRotateYFixRUseAMap(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}

// Features: X-axis rotation
DLL_PUBLIC void CopyRotateX(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyRotateXUseAMap(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyRotateXFixU(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyRotateXFixD(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyRotateXFixUUseAMap(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyRotateXFixDUseAMap(int nWrite, int nDest, int nSrc, int nSx, int nSy, int nWidth, int nHeight, float fRotate, float fMag){std::cout << "DrawGraph:???" << std::endl;}

// Features: inverted
DLL_PUBLIC void CopyReverseLR(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyReverseUD(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyReverseAMapLR(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyReverseAMapUD(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight){std::cout << "DrawGraph:???" << std::endl;}

// Features: blur
DLL_PUBLIC void CopyWidthBlur(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nBlur){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyHeightBlur(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nBlur){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyAMapWidthBlur(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nBlur){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void CopyAMapHeightBlur(int nDest, int nDx, int nDy, int nSrc, int nSx, int nSy, int nWidth, int nHeight, int nBlur){std::cout << "DrawGraph:???" << std::endl;}

// Features: line drawing
DLL_PUBLIC void DrawLine(int nDest, int nX0, int nY0, int nX1, int nY1, int nR, int nG, int nB){std::cout << "DrawGraph:???" << std::endl;}
DLL_PUBLIC void DrawLineToAMap(int nDest, int nX0, int nY0, int nX1, int nY1, int nAlpha){std::cout << "DrawGraph:???" << std::endl;}

// Features: value obtained
DLL_PUBLIC bool GetPixelColor(int nSurface, int nX, int nY, int nR, int nG, int nB)
{
    std::cout << "DrawGraph:???" << std::endl;
    return 0; // TODO
}
DLL_PUBLIC bool GetAlphaColor(int nSurface, int nX, int nY, int nA)
{
    std::cout << "DrawGraph:???" << std::endl;
    return 0; // TODO
}

// Features: polygon drawing
DLL_PUBLIC void DrawPolygon(int nDest, int nTex,
       float fX0, float fY0, float fZ0, float fU0, float fV0,
       float fX1, float fY1, float fZ1, float fU1, float fV1,
       float fX2, float fY2, float fZ2, float fU2, float fV2){std::cout << "DrawGraph:DrawPolygon" << std::endl;}

DLL_PUBLIC void DrawColorPolygon(int nDest,
     float fX0, float fY0, float fZ0, int nR0, int nG0, int nB0, int nA0,
     float fX1, float fY1, float fZ1, int nR1, int nG1, int nB1, int nA1,
     float fX2, float fY2, float fZ2, int nR2, int nG2, int nB2, int nA2){std::cout << "DrawGraph:DrawColorPolygon" << std::endl;}


/////////////////////////////////////////////////////////////////////////////////

DLL_PUBLIC IInterface* CreateInterface(const void *a1)
{
    std::cout << "DrawGraph:CreateInterface" << std::endl;
    //return (unsigned int)&unk_1001B418 & ((memcmp(a1, &unk_100182F8, 16) != 0) - 1);
    IInterface* ifc;
    return ifc; // TODO THIS IS THE MOST IMPORTANT THING
}

DLL_PUBLIC char* FillOverlay(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8)
{
    std::cout << "DrawGraph:FillOverlay" << std::endl;
    //return sub_10003450((int)&unk_1001B418, a1, a2, a3, a4, a5, a6, a7, a8);
    char* r = (char*)"return";
    return r;
}

DLL_PUBLIC char* FillOverlayAlpha(int a1, int a2, int a3, int a4, int a5, signed int a6, signed int a7, int a8, int a9)
{
    std::cout << "DrawGraph:FillOverlayAlpha" << std::endl;
    //return sub_10003570((int)&unk_1001B418, a1, a2, a3, a4, a5, a6, a7, a8, a9);
    char* r = (char*)"return";
    return r;
}


