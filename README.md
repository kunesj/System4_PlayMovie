PlayMovie library
=================
Library (with stub methods) that is replacement for Alicesoft's System4.0 PlayMovie.dll library. Useful when Wine is not able play ingame movies.

Download compiled: https://gitlab.com/kunesj/System4_PlayMovie/-/tags

Exported methods
----------------

    bool Init( void );
    bool IsPlay( void ); 
    bool Load( char* szFileName );
    bool Play( void );
    bool Release( void ); 
    bool Stop( void );
    
Cross compile from Linux (Debian/Ubuntu) for Windows
----------------------------------------------------

Install compiler and build tools   
    
    sudo apt install cmake binutils-mingw-w64-i686 gcc-mingw-w64-i686 g++-mingw-w64-i686

From source directory run to compile: (cmake needs to be run 2x)

    mkdir build
    cd build
    cmake -DCMAKE_TOOLCHAIN_FILE=../mingw-w64-toolchain.cmake ..
    make 

